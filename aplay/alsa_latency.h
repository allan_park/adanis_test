/*
 * Adanis Inc.
 *   #2015~2016, Geumkang Penterium IT Tower B-Towe 282, Hakuro, Dongan-Gu, Anyang-Si, Kyunggi-Do, Korea (431-060)
 *
 **  Name:          alsa_latency.h
 **
 **  Description:   Link(or Connect) alsa capturea and alsa playback device
 **
 **  Modifier:      ADANIS YCJUNG
 **
 **  Original Source Code
 **      http://www.alsa-project.org/alsa-doc/alsa-lib/_2test_2latency_8c-example.html
 **
 **  History
 **     Date 08/11/2016
 *          => first release
 *          => now fixed device("hw:0,2", "hw:0,0") linking.
 *
 */
#ifndef __ALSA_LATENCY_H__
#define __ALSA_LATENCY_H__

typedef enum {
  EX_ALSA_STOP = 0x00,
  EX_ALSA_GO = 0x01,
} tEX_ALSA_GOSTOP;

/*******************************************************************************
 **
 ** Function         alsa_sleep_ms
 **
 ** Description      just milliseconds sleep function
 **
 ** Parameter        
 **
 ** Returns          
 *******************************************************************************/
void alsa_sleep_ms(int milliseconds);

/*******************************************************************************
 **
 ** Function         alsa_link_capture_playback
 **
 ** Description      link(connect) device("hw:0,2", "hw:0,0")
 **                  (*) blocking funciton, so call this function on thread or run-loop
 **
 ** Parameter        [IN] _stop_flag    - woking stop flag
 **                  [IN] playback_rate - 16000 or 8000
 **
 ** Returns          0 if successful execution, error code else
 *******************************************************************************/
int  alsa_link_capture_playback(tEX_ALSA_GOSTOP *_stop_flag, int playback_rate);
#endif

