/*
 *  Latency test program
 *
 *     Author: Jaroslav Kysela <perex@perex.cz>
 *
 *     Author of bandpass filter sweep effect:
 *             Maarten de Boer <mdeboer@iua.upf.es>
 *
 *  This small demo program can be used for measuring latency between
 *  capture and playback. This latency is measured from driver (diff when
 *  playback and capture was started). Scheduler is set to SCHED_RR.
 *
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 */
/*
 * Adanis Inc.
 *   #2015~2016, Geumkang Penterium IT Tower B-Towe 282, Hakuro, Dongan-Gu, Anyang-Si, Kyunggi-Do, Korea (431-060)
 *
 **  Name:          alsa_latency.c
 **
 **  Description:   Link(or Connect) alsa capturea and alsa playback device
 **
 **  Modifier:      ADANIS YCJUNG
 **
 **  Original Source Code
 **      http://www.alsa-project.org/alsa-doc/alsa-lib/_2test_2latency_8c-example.html
 **
 **  History
 **     Date 08/11/2016
 *          => first release
 *          => now fixed device("hw:0,2", "hw:0,0") linking.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sched.h>
#include <errno.h>
#ifdef __GETOPT__
    #include <getopt.h>
#endif
#if 0 /* ORG CODE */
    #include "../include/asoundlib.h"
#else
    #include <alsa/asoundlib.h>
#endif
#include <sys/time.h>
#include <math.h>

/* http://stackoverflow.com/questions/1157209/is-there-an-alternative-sleep-function-in-c-to-milliseconds */
#ifdef WIN32
    #include <windows.h>
#elif _POSIX_C_SOURCE >= 199309L
    #include <time.h>   // for nanosleep
#else
    #include <unistd.h> // for usleep
#endif

#include "alsa_latency.h"

#define MY_DEVICE_A_FROM "hw:0,2"
#define MY_DEVICE_A_TO   "hw:0,0" /* or "default" */
#define MY_DEVICE_B_FROM MY_DEVICE_A_TO
#define MY_DEVICE_B_TO   MY_DEVICE_A_FROM

static char *pdevice = MY_DEVICE_A_FROM;   /* i2s to local */
static char *cdevice = MY_DEVICE_A_TO;

#define __ADD_DEVICE_B__
#define __REMOVE_EXIT_CALL__
#define __NO_MAIN__

static char *pdevice_b = MY_DEVICE_B_FROM; /* locatl to i2s */
static char *cdevice_b = MY_DEVICE_B_TO;

static snd_pcm_format_t format = SND_PCM_FORMAT_S16_LE;
static int rate                = 16000; // 22050;
static int channels            = 0x02;

/*
 * PERIOD_TIME    : [500 128000]
 * PERIOD_SIZE    : [8 2048]
 * PERIOD_BYTES   : [32 8192]
 * PERIODS        : [2 64]
 * BUFFER_TIME    : [1000 2048000]
 * BUFFER_SIZE    : [16 32768]
 * BUFFER_BYTES   : [64 131072]
 */
static int buffer_size = 0x00;            /* auto */
static int period_size = 0x00;            /* auto */
static int latency_min = 2000; // 32;  /* in frames / 2 */
static int latency_max = 2048;         /* in frames / 2 */
// Trying latency 2000 frames, 125000.000us, 125.000000ms (8.0000Hz)
// Trying latency 4000 frames, 250000.000us, 250.000000ms (4.0000Hz)
static int loop_sec = 30;              /* seconds */
static int block    = 0x00;            /* block mode */
static int use_poll = 0x00;
static int resample = 0x01;
static unsigned long loop_limit;
static snd_output_t *output = NULL;

static int dump_hw_params = 0x00;

typedef enum {
  EX_LATENCY_RTN_SUCCESS = 0x00,
  EX_LATENCY_RTN_ERROR   = -1,
  EX_LATENCY_RTN_EXIT    = -2,
} tEX_LATENCY;

#ifdef __REMOVE_EXIT_CALL__
  #define CALL_EXIT(__x__) do {\
    return EX_LATENCY_RTN_EXIT; \
  } while (0)
#else
  #define CALL_EXIT(__x__) do {\
	exit(__x__); \
  } while (0)
#endif

static int setparams_stream(snd_pcm_t *handle,
                     snd_pcm_hw_params_t *params,
                     const char *id) {
    int err;
    unsigned int rrate;
    err = snd_pcm_hw_params_any(handle, params);
    if (err < 0) {
        printf("Broken configuration for %s PCM: no configurations available: %s\n", snd_strerror(err), id);
        return err;
    }
    err = snd_pcm_hw_params_set_rate_resample(handle, params, resample);
    if (err < 0) {
        printf("Resample setup failed for %s (val %i): %s\n", id, resample, snd_strerror(err));
        return err;
    }
    err = snd_pcm_hw_params_set_access(handle, params, SND_PCM_ACCESS_RW_INTERLEAVED);
    if (err < 0) {
        printf("Access type not available for %s: %s\n", id, snd_strerror(err));
        return err;
    }
    err = snd_pcm_hw_params_set_format(handle, params, format);
    if (err < 0) {
        printf("Sample format not available for %s: %s\n", id, snd_strerror(err));
        return err;
    }
    err = snd_pcm_hw_params_set_channels(handle, params, channels);
    if (err < 0) {
        printf("Channels count (%i) not available for %s: %s\n", channels, id, snd_strerror(err));
        return err;
    }
    rrate = rate;
    err = snd_pcm_hw_params_set_rate_near(handle, params, &rrate, 0);
    if (err < 0) {
        printf("Rate %iHz not available for %s: %s\n", rate, id, snd_strerror(err));
        return err;
    }
    if ((int)rrate != rate) {
        printf("Rate doesn't match (requested %iHz, get %iHz)\n", rate, err);
        return -EINVAL;
    }
    return 0;
}

static int setparams_bufsize(snd_pcm_t *handle,
                      snd_pcm_hw_params_t *params,
                      snd_pcm_hw_params_t *tparams,
                      snd_pcm_uframes_t bufsize,
                      const char *id) {
    int err;
    snd_pcm_uframes_t periodsize;
    snd_pcm_hw_params_copy(params, tparams);
    periodsize = bufsize * 2;
    err = snd_pcm_hw_params_set_buffer_size_near(handle, params, &periodsize);
    if (err < 0) {
        printf("Unable to set buffer size %li for %s: %s\n", bufsize * 2, id, snd_strerror(err));
        return err;
    }
    if (period_size > 0)
        periodsize = period_size;
    else
        periodsize /= 2;
    err = snd_pcm_hw_params_set_period_size_near(handle, params, &periodsize, 0);
    if (err < 0) {
        printf("Unable to set period size %li for %s: %s\n", periodsize, id, snd_strerror(err));
        return err;
    }
    return 0;
}

static int setparams_set(snd_pcm_t *handle,
                  snd_pcm_hw_params_t *params,
                  snd_pcm_sw_params_t *swparams,
                  const char *id)
{
    int err;
    snd_pcm_uframes_t val;
    err = snd_pcm_hw_params(handle, params);
    if (err < 0) {
        printf("Unable to set hw params for %s: %s\n", id, snd_strerror(err));
        return err;
    }
    err = snd_pcm_sw_params_current(handle, swparams);
    if (err < 0) {
        printf("Unable to determine current swparams for %s: %s\n", id, snd_strerror(err));
        return err;
    }
    err = snd_pcm_sw_params_set_start_threshold(handle, swparams, 0x7fffffff);
    if (err < 0) {
        printf("Unable to set start threshold mode for %s: %s\n", id, snd_strerror(err));
        return err;
    }
    if (!block) {
        val = 4;
    } else {
        snd_pcm_hw_params_get_period_size(params, &val, NULL);
        printf("[info] setparams_set :: period_size[%d]\n", (int)val);
    }
    err = snd_pcm_sw_params_set_avail_min(handle, swparams, val);
    if (err < 0) {
        printf("Unable to set avail min for %s: %s\n", id, snd_strerror(err));
        return err;
    }
    err = snd_pcm_sw_params(handle, swparams);
    if (err < 0) {
        printf("Unable to set sw params for %s: %s\n", id, snd_strerror(err));
        return err;
    }
    return 0;
}

static int setparams(snd_pcm_t *phandle, snd_pcm_t *chandle, int *bufsize) {
    int err, last_bufsize = *bufsize;
    snd_pcm_hw_params_t *pt_params, *ct_params;     /* templates with rate, format and channels */
    snd_pcm_hw_params_t *p_params, *c_params;
    snd_pcm_sw_params_t *p_swparams, *c_swparams;
    snd_pcm_uframes_t p_size, c_size, p_psize, c_psize;
    unsigned int p_time, c_time;
    unsigned int val;
    snd_pcm_hw_params_alloca(&p_params);
    snd_pcm_hw_params_alloca(&c_params);
    snd_pcm_hw_params_alloca(&pt_params);
    snd_pcm_hw_params_alloca(&ct_params);
    snd_pcm_sw_params_alloca(&p_swparams);
    snd_pcm_sw_params_alloca(&c_swparams);
    if ((err = setparams_stream(phandle, pt_params, "playback")) < 0) {
        printf("Unable to set parameters for playback stream: %s\n", snd_strerror(err));
        CALL_EXIT(0x00);
    }
    if ((err = setparams_stream(chandle, ct_params, "capture")) < 0) {
        printf("Unable to set parameters for capture stream: %s\n", snd_strerror(err));
        CALL_EXIT(0x00);
    }
    if (buffer_size > 0) {
        *bufsize = buffer_size;
        goto __set_it;
    }
  __again:
    if (buffer_size > 0) {
        printf("[error] buffer_size:%d\n", buffer_size);
        return EX_LATENCY_RTN_ERROR/*-1*/;
    }
    if (last_bufsize == *bufsize)
        *bufsize += 4;
    last_bufsize = *bufsize;
    if (*bufsize > latency_max) {
        printf("[error]parameters last_bufsize:%d, latency_max:%d\n", last_bufsize, latency_max);
        return EX_LATENCY_RTN_ERROR/*-1*/;
    }
  __set_it:
    if ((err = setparams_bufsize(phandle, p_params, pt_params, *bufsize, "playback")) < 0) {
        printf("Unable to set sw parameters for playback stream: %s\n", snd_strerror(err));
        CALL_EXIT(0x00);
    }
    if ((err = setparams_bufsize(chandle, c_params, ct_params, *bufsize, "capture")) < 0) {
        printf("Unable to set sw parameters for playback stream: %s\n", snd_strerror(err));
        CALL_EXIT(0x00);
    }
/*
 * <<pc>>
 * [info] period_size :: p_params[ 32], c_params[ 32]
 * [info] period_time :: p_params[725], c_params[725]
 * [info] buffer_size :: p_params[ 64], c_params[ 64]
 * 
 * <<target>>
 * [info] period_size :: p_params[0], c_params[0]
 * [info] period_time :: p_params[0], c_params[0]
 * [info] buffer_size :: p_params[-1093449176], c_params[70801]
*/
    snd_pcm_hw_params_get_period_size(p_params, &p_psize, NULL);
    snd_pcm_hw_params_get_period_size(c_params, &c_psize, NULL);
#if 1 /* __PERIOD_TIME_PERIOD_SIZE_BUFFER_SIZE__ */
    // printf("[info] period_size :: p_params[%d], c_params[%d]\n", p_psize, c_psize);
    p_psize = 32;
    c_psize = 32;
#endif
    if (p_psize > (unsigned int)*bufsize) {
        *bufsize = p_psize;
    }
    
    // snd_pcm_hw_params_get_period_size(c_params, &c_psize, NULL);
    if (c_psize > (unsigned int)*bufsize) {
        *bufsize = c_psize;
    }
    
    snd_pcm_hw_params_get_period_time(p_params, &p_time, NULL);
    snd_pcm_hw_params_get_period_time(c_params, &c_time, NULL);
#if 1 /* __PERIOD_TIME_PERIOD_SIZE_BUFFER_SIZE__ */
    // printf("[info] period_time :: p_params[%d], c_params[%d]\n", p_time, c_time);
    p_time = 725;
    c_time = 725;
#endif
    if (p_time != c_time) {
        goto __again;
    }
    
    snd_pcm_hw_params_get_buffer_size(p_params, &p_size);
    snd_pcm_hw_params_get_buffer_size(c_params, &c_size);
#if 1 /* __PERIOD_TIME_PERIOD_SIZE_BUFFER_SIZE__ */
    // printf("[info] buffer_size :: p_params[%d], c_params[%d]\n", p_size, c_size);
    p_size = 64;
    c_size = 64;
#endif
    if (p_psize * 2 < p_size) {
        snd_pcm_hw_params_get_periods_min(p_params, &val, NULL);
        if (val > 2) {
            printf("playback device does not support 2 periods per buffer\n");
            CALL_EXIT(0x00);
        }
        goto __again;
    }
    // snd_pcm_hw_params_get_buffer_size(c_params, &c_size);
    if (c_psize * 2 < c_size) {
        snd_pcm_hw_params_get_periods_min(c_params, &val, NULL);
        if (val > 2 ) {
            printf("capture device does not support 2 periods per buffer\n");
            CALL_EXIT(0x00);
        }
        goto __again;
    }
    if ((err = setparams_set(phandle, p_params, p_swparams, "playback")) < 0) {
        printf("Unable to set sw parameters for playback stream: %s\n", snd_strerror(err));
        CALL_EXIT(0x00);
    }
    if ((err = setparams_set(chandle, c_params, c_swparams, "capture")) < 0) {
        printf("Unable to set sw parameters for playback stream: %s\n", snd_strerror(err));
        CALL_EXIT(0x00);
    }
    if ((err = snd_pcm_prepare(phandle)) < 0) {
        printf("Prepare error: %s\n", snd_strerror(err));
        CALL_EXIT(0x00);
    }

    if(dump_hw_params) {
        snd_pcm_dump(phandle, output);
        snd_pcm_dump(chandle, output);
    }
    fflush(stdout);
    return EX_LATENCY_RTN_SUCCESS/*0*/;
}

static void showstat(snd_pcm_t *handle, size_t frames) {
    int err;
    snd_pcm_status_t *status;
    snd_pcm_status_alloca(&status);
    if ((err = snd_pcm_status(handle, status)) < 0) {
        printf("Stream status error: %s\n", snd_strerror(err));
        exit(0);
    }
    printf("*** frames = %li ***\n", (long)frames);
    if(dump_hw_params) {
        snd_pcm_status_dump(status, output);
    }
}

static void showlatency(size_t latency) {
    double d;
    latency *= 2;
    d = (double)latency / (double)rate;
    printf("Trying latency %li frames, %.3fus, %.6fms (%.4fHz)\n", (long)latency, d * 1000000, d * 1000, (double)1 / d);
}

static void showinmax(size_t in_max) {
    double d;
    printf("Maximum read: %li frames\n", (long)in_max);
    d = (double)in_max / (double)rate;
    printf("Maximum read latency: %.3fus, %.6fms (%.4fHz)\n", d * 1000000, d * 1000, (double)1 / d);
}

static void gettimestamp(snd_pcm_t *handle, snd_timestamp_t *timestamp) {
    int err;
    snd_pcm_status_t *status;
    snd_pcm_status_alloca(&status);
    if ((err = snd_pcm_status(handle, status)) < 0) {
        printf("Stream status error: %s\n", snd_strerror(err));
        exit(0);
    }
    snd_pcm_status_get_trigger_tstamp(status, timestamp);
}

static void setscheduler(void) {
    struct sched_param sched_param;
    if (sched_getparam(0, &sched_param) < 0) {
        printf("Scheduler getparam failed...\n");
        return;
    }
    sched_param.sched_priority = sched_get_priority_max(SCHED_RR);
    if (!sched_setscheduler(0, SCHED_RR, &sched_param)) {
        printf("Scheduler set to Round Robin with priority %i...\n", sched_param.sched_priority);
        fflush(stdout);
        return;
    }
    printf("!!!Scheduler set to Round Robin with priority %i FAILED!!!\n", sched_param.sched_priority);
}

static long timediff(snd_timestamp_t t1, snd_timestamp_t t2) {
    signed long l;
    t1.tv_sec -= t2.tv_sec;
    l = (signed long) t1.tv_usec - (signed long) t2.tv_usec;
    if (l < 0) {
        t1.tv_sec--;
        l = 1000000 + l;
        l %= 1000000;
    }
    return (t1.tv_sec * 1000000) + l;
}

static long readbuf(snd_pcm_t *handle, char *buf, long len, size_t *frames, size_t *max) {
    long r;
    if (!block) {
        do {
            r = snd_pcm_readi(handle, buf, len);
        } while (r == -EAGAIN);
        if (r > 0) {
            *frames += r;
            if ((long)*max < r)
                *max = r;
        }
        // printf("read = %li\n", r);
    } else {
        int frame_bytes = (snd_pcm_format_width(format) / 8) * channels;
        do {
            r = snd_pcm_readi(handle, buf, len);
            if (r > 0) {
                buf += r * frame_bytes;
                len -= r;
                *frames += r;
                if ((long)*max < r)
                    *max = r;
            }
            // printf("r = %li, len = %li\n", r, len);
        } while (r >= 1 && len > 0);
    }
    // showstat(handle, 0);
    return r;
}

static long writebuf(snd_pcm_t *handle, char *buf, long len, size_t *frames) {
    long r;
    while (len > 0) {
        r = snd_pcm_writei(handle, buf, len);
        if (r == -EAGAIN)
                continue;
        // printf("write = %li\n", r);
        if (r < 0)
                return r;
        // showstat(handle, 0);
        buf += r * 4;
        len -= r;
        *frames += r;
    }
    return 0;
}

#ifdef __EFFECT__
#define FILTERSWEEP_LFO_CENTER 2000.
#define FILTERSWEEP_LFO_DEPTH 1800.
#define FILTERSWEEP_LFO_FREQ 0.2
#define FILTER_BANDWIDTH 50
/* filter the sweep variables */
static float lfo,dlfo,fs,fc,BW,C,D,a0,a1,a2,b1,b2,*x[3],*y[3];
static void applyeffect(char* buffer,int r) {
    short* samples = (short*) buffer;
    int i;
    for (i=0;i<r;i++) {
        int chn;
        fc = sin(lfo)*FILTERSWEEP_LFO_DEPTH+FILTERSWEEP_LFO_CENTER;
        lfo += dlfo;
        if (lfo>2.*M_PI) lfo -= 2.*M_PI;
        C = 1./tan(M_PI*BW/fs);
        D = 2.*cos(2*M_PI*fc/fs);
        a0 = 1./(1.+C);
        a1 = 0;
        a2 = -a0;
        b1 = -C*D*a0;
        b2 = (C-1)*a0;
        for (chn=0;chn<channels;chn++) {
            x[chn][2] = x[chn][1];
            x[chn][1] = x[chn][0];
            y[chn][2] = y[chn][1];
            y[chn][1] = y[chn][0];
            x[chn][0] = samples[i*channels+chn];
            y[chn][0] = a0*x[chn][0] + a1*x[chn][1] + a2*x[chn][2] 
                    - b1*y[chn][1] - b2*y[chn][2];
            samples[i*channels+chn] = y[chn][0];
        }
    }
}
#endif /* endif __EFFECT__ */

#ifdef __NO_MAIN__
    /* NO NEED */
#else
static void help(void) {
    int k;
    printf(
"Usage: latency [OPTION]... [FILE]...\n"
"-h,--help      help\n"
"-P,--pdevice   playback device\n"
"-C,--cdevice   capture device\n"
"-m,--min       minimum latency in frames\n"
"-M,--max       maximum latency in frames\n"
"-F,--frames    frames to transfer\n"
"-f,--format    sample format\n"
"-c,--channels  channels\n"
"-r,--rate      rate\n"
"-B,--buffer    buffer size in frames\n"
"-E,--period    period size in frames\n"
"-s,--seconds   duration of test in seconds\n"
"-b,--block     block mode\n"
"-p,--poll      use poll (wait for event - reduces CPU usage)\n"
#ifdef __EFFECT__
"-e,--effect    apply an effect (bandpass filter sweep)\n"
#endif
);
    printf("Recognized sample formats are:");
    for (k = 0; k < SND_PCM_FORMAT_LAST; ++k) {
        const char *s = snd_pcm_format_name(k);
        if (s)
            printf(" %s", s);
    }
    printf("\n\n");
    printf(
"Tip #1 (usable latency with large periods, non-blocking mode, good CPU usage,\n"
"        superb xrun prevention):\n"
"  latency -m 8192 -M 8192 -t 1 -p\n"
"Tip #2 (superb latency, non-blocking mode, but heavy CPU usage):\n"
"  latency -m 128 -M 128\n"
);
}
#endif

static void free_pcm_buffer(void *buffer_a, void *buffer_b) {
    if(buffer_a) {
        free(buffer_a);
        buffer_a = NULL;
    }
    
    if(buffer_b) {
        free(buffer_b);
        buffer_b = NULL;
    }
}


static void free_pcm_handle(snd_pcm_t *phandle  , snd_pcm_t *chandle, 
                             snd_pcm_t *phandle_b, snd_pcm_t *chandle_b) {
    if(phandle) {
        snd_pcm_hw_free(phandle);
    }

    if(chandle) {
        snd_pcm_unlink(chandle);
        snd_pcm_hw_free(chandle);
    }

    if(phandle_b) {
        snd_pcm_hw_free(phandle_b);
    }

    if(chandle_b) {
        snd_pcm_unlink(chandle_b);
        snd_pcm_hw_free(chandle_b);
    }
}


static void close_pcm_handle(snd_pcm_t *phandle  , snd_pcm_t *chandle, 
                             snd_pcm_t *phandle_b, snd_pcm_t *chandle_b) {
    if(phandle) {
        snd_pcm_close(phandle);  phandle = NULL;
    }

    if(chandle) {
        snd_pcm_close(chandle);  chandle = NULL;
    }

    if(phandle_b) {
        snd_pcm_close(phandle_b); phandle_b = NULL;
    }

    if(chandle_b) {
        snd_pcm_close(chandle_b); chandle_b = NULL;
    }
}

#ifdef __NO_MAIN__
    /* NO NEED */
#else  /* else __NO_MAIN__ */
int main(int argc, char *argv[]) {
#ifdef __GETOPT__
    struct option long_option[] = {
        {"help", 0, NULL, 'h'},
        {"pdevice", 1, NULL, 'P'},
        {"cdevice", 1, NULL, 'C'},
        {"min", 1, NULL, 'm'},
        {"max", 1, NULL, 'M'},
        {"frames", 1, NULL, 'F'},
        {"format", 1, NULL, 'f'},
        {"channels", 1, NULL, 'c'},
        {"rate", 1, NULL, 'r'},
        {"buffer", 1, NULL, 'B'},
        {"period", 1, NULL, 'E'},
        {"seconds", 1, NULL, 's'},
        {"block", 0, NULL, 'b'},
        {"poll", 0, NULL, 'p'},
#ifdef __EFFECT__
        {"effect", 0, NULL, 'e'},
#endif
        {NULL, 0, NULL, 0},
    };
#endif /* endif __GETOPT__ */

    snd_pcm_t *phandle = NULL;
    snd_pcm_t *chandle = NULL;
// #ifdef __ADD_DEVICE_B__
    snd_pcm_t *phandle_b = NULL;
    snd_pcm_t *chandle_b = NULL;
// #endif
    char *buffer_b = NULL;
    char *buffer   = NULL;
    int err, latency, morehelp;
    int ok, rtn_err;
    snd_timestamp_t p_tstamp, c_tstamp;
    ssize_t r;
    size_t frames_in, frames_out, in_max;
#ifdef __EFFECT__
    int effect = 0;
#endif

#ifdef __GETOPT__
    morehelp = 0;
    while (1) {
        int c;
        if ((c = getopt_long(argc, argv, "hP:C:m:M:F:f:c:r:B:E:s:bpen", long_option, NULL)) < 0)
                break;
        switch (c) {
        case 'h':
            morehelp++;
            break;
        case 'P':
            pdevice = strdup(optarg);
            break;
        case 'C':
            cdevice = strdup(optarg);
            break;
        case 'm':
            err = atoi(optarg) / 2;
            latency_min = err >= 4 ? err : 4;
            if (latency_max < latency_min)
                    latency_max = latency_min;
            break;
        case 'M':
            err = atoi(optarg) / 2;
            latency_max = latency_min > err ? latency_min : err;
            break;
        case 'f':
            format = snd_pcm_format_value(optarg);
            if (format == SND_PCM_FORMAT_UNKNOWN) {
                printf("Unknown format, setting to default S16_LE\n");
                format = SND_PCM_FORMAT_S16_LE;
            }
            break;
        case 'c':
            err = atoi(optarg);
            channels = err >= 1 && err < 1024 ? err : 1;
            break;
        case 'r':
            err = atoi(optarg);
            rate = err >= 4000 && err < 200000 ? err : 44100;
            break;
        case 'B':
            err = atoi(optarg);
            buffer_size = err >= 32 && err < 200000 ? err : 0;
            break;
        case 'E':
            err = atoi(optarg);
            period_size = err >= 32 && err < 200000 ? err : 0;
            break;
        case 's':
            err = atoi(optarg);
            loop_sec = err >= 1 && err <= 100000 ? err : 30;
            break;
        case 'b':
            block = 1;
            break;
        case 'p':
            use_poll = 1;
            break;
#ifdef __EFFECT__
        case 'e':
            effect = 1;
            break;
#endif
        case 'n':
            resample = 0;
            break;
        }
    }
    if (morehelp) {
        help();
        return 0;
    }
#endif /* endif __GETOPT__ */

    if(dump_hw_params) {
        err = snd_output_stdio_attach(&output, stdout, 0);
        if (err < 0) {
            printf("Output failed: %s\n", snd_strerror(err));
            return 0;
        }
    }
    loop_limit = loop_sec * rate;
    latency    = latency_min - 4;
    buffer     = malloc((latency_max * snd_pcm_format_width(format) / 8) * 2);
    if(buffer == NULL) {
        printf("[error] no memory !!\n");
        return 0x00;
    }
    
#ifdef __ADD_DEVICE_B__
    buffer_b   = malloc((latency_max * snd_pcm_format_width(format) / 8) * 2);
    if(buffer_b == NULL) {
        free_pcm_buffer(buffer, buffer_b);
        printf("[error] no memory[B] !!\n");
        return 0x00;
    }
#endif

    setscheduler();
    printf("Playback device is %s\n", pdevice);
    printf("Capture device is %s\n", cdevice);
    printf("Parameters are %iHz, %s, %i channels, %s mode\n", rate, snd_pcm_format_name(format), channels, block ? "blocking" : "non-blocking");
    printf("Poll mode: %s\n", use_poll ? "yes" : "no");
    printf("Loop limit is %li frames, minimum latency = %i, maximum latency = %i\n", loop_limit, latency_min * 2, latency_max * 2);
    if ((err = snd_pcm_open(&phandle, pdevice, SND_PCM_STREAM_PLAYBACK, block ? 0 : SND_PCM_NONBLOCK)) < 0) {
        printf("Playback open error: %s\n", snd_strerror(err));
        free_pcm_buffer(buffer, buffer_b);
        close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
        return err;
    }
    if ((err = snd_pcm_open(&chandle, cdevice, SND_PCM_STREAM_CAPTURE, block ? 0 : SND_PCM_NONBLOCK)) < 0) {
        printf("Record open error: %s\n", snd_strerror(err));
        free_pcm_buffer(buffer, buffer_b);
        close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
        return err;
    }

#ifdef __ADD_DEVICE_B__
    if ((err = snd_pcm_open(&phandle_b, pdevice_b, SND_PCM_STREAM_PLAYBACK, block ? 0 : SND_PCM_NONBLOCK)) < 0) {
        printf("Playback(B) open error: %s\n", snd_strerror(err));
        free_pcm_buffer(buffer, buffer_b);
        close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
        return err;
    }
    if ((err = snd_pcm_open(&chandle_b, cdevice_b, SND_PCM_STREAM_CAPTURE, block ? 0 : SND_PCM_NONBLOCK)) < 0) {
        printf("Record(B) open error: %s\n", snd_strerror(err));
        free_pcm_buffer(buffer, buffer_b);
        close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
        return err;
    }
#endif
    
#ifdef __EFFECT__
    /* initialize the filter sweep variables */
    if (effect) {
        fs = (float) rate;
        BW = FILTER_BANDWIDTH;
        lfo = 0;
        dlfo = 2.*M_PI*FILTERSWEEP_LFO_FREQ/fs;
        x[0] = (float*) malloc(channels*sizeof(float));         
        x[1] = (float*) malloc(channels*sizeof(float));         
        x[2] = (float*) malloc(channels*sizeof(float));         
        y[0] = (float*) malloc(channels*sizeof(float));         
        y[1] = (float*) malloc(channels*sizeof(float));         
        y[2] = (float*) malloc(channels*sizeof(float));         
    }
#endif /* endif __EFFECT__ */
    // printf("AAA - 001\n");
    while (1) {
        frames_in = frames_out = 0;
        if ((rtn_err = setparams(phandle, chandle, &latency)) < 0) {
            printf("[error] setparams\n");

#ifdef __REMOVE_EXIT_CALL__
            if(rtn_err == EX_LATENCY_RTN_EXIT) {
                free_pcm_buffer(buffer, buffer_b);
                close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
                return 0x00;
            }
#endif
            break;
        }
#ifdef __ADD_DEVICE_B__
        if ((rtn_err = setparams(phandle_b, chandle_b, &latency)) < 0) {
            printf("[error] setparams[B]\n");
#ifdef __REMOVE_EXIT_CALL__
            if(rtn_err == EX_LATENCY_RTN_EXIT) {
                free_pcm_buffer(buffer, buffer_b);
                close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
                return 0x00;
            }
#endif
            break;
        }
#endif
        showlatency(latency);
        if ((err = snd_pcm_link(chandle, phandle)) < 0) {
            printf("Streams link error: %s\n", snd_strerror(err));
            free_pcm_buffer(buffer, buffer_b);
#ifdef __REMOVE_EXIT_CALL__
            close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
            return 0x00;
#else
            exit(0);
#endif
        }
#ifdef __ADD_DEVICE_B__
        if ((err = snd_pcm_link(chandle_b, phandle_b)) < 0) {
            printf("Streams link error[B]: %s\n", snd_strerror(err));
            free_pcm_buffer(buffer, buffer_b);
#ifdef __REMOVE_EXIT_CALL__
            free_pcm_handle(phandle, chandle, phandle_b, chandle_b);
            close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
            return 0x00;
#else
            exit(0);
#endif
        }
#endif
        if (snd_pcm_format_set_silence(format, buffer, latency*channels) < 0) {
            fprintf(stderr, "silence error\n");
            break;
        }
        if (writebuf(phandle, buffer, latency, &frames_out) < 0) {
            fprintf(stderr, "write error[01]\n");
            break;
        }
        if (writebuf(phandle, buffer, latency, &frames_out) < 0) {
            fprintf(stderr, "write error[02]\n");
            break;
        }
        if ((err = snd_pcm_start(chandle)) < 0) {
            printf("Go error: %s\n", snd_strerror(err));
            free_pcm_buffer(buffer, buffer_b);
#ifdef __REMOVE_EXIT_CALL__
            free_pcm_handle(phandle, chandle, phandle_b, chandle_b);
            close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
            return 0x00;
#else
            exit(0);
#endif
        }
#ifdef __ADD_DEVICE_B__
        if (writebuf(phandle_b, buffer_b, latency, &frames_out) < 0) {
            fprintf(stderr, "write error[B][01]\n");
            break;
        }
        if (writebuf(phandle_b, buffer_b, latency, &frames_out) < 0) {
            fprintf(stderr, "write error[B][02]\n");
            break;
        }
        if ((err = snd_pcm_start(chandle_b)) < 0) {
            printf("Go error[B]: %s\n", snd_strerror(err));
            free_pcm_buffer(buffer, buffer_b);
#ifdef __REMOVE_EXIT_CALL__
            free_pcm_handle(phandle, chandle, phandle_b, chandle_b);
            close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
            return 0x00;
#else
            exit(0);
#endif
        }
#endif
        gettimestamp(phandle, &p_tstamp);
        gettimestamp(chandle, &c_tstamp);
#if 0
        printf("Playback:\n");
        showstat(phandle, frames_out);
        printf("Capture:\n");
        showstat(chandle, frames_in);
#endif
        ok = 1;
        in_max = 0;
        
        // printf("AAA - 002\n");
        while (ok && frames_in < loop_limit) {
            if (use_poll) {
                /* use poll to wait for next event */
                snd_pcm_wait(chandle, 1000);
            }
            if ((r = readbuf(chandle, buffer, latency, &frames_in, &in_max)) < 0)
                ok = 0;
            else {
#ifdef __EFFECT__
                if (effect)
                    applyeffect(buffer,r);
#endif
                if (writebuf(phandle, buffer, r, &frames_out) < 0)
                    ok = 0;
            }

#ifdef __ADD_DEVICE_B__
            if ((r = readbuf(chandle_b, buffer_b, latency, &frames_in, &in_max)) < 0)
                ok = 0;
            else {
#ifdef __EFFECT__
                if (effect)
                    applyeffect(buffer_b,r);
#endif
                if (writebuf(phandle_b, buffer_b, r, &frames_out) < 0)
                    ok = 0;
            }
#endif
        }
        if (ok)
            printf("Success\n");
        else
            printf("Failure\n");
        printf("Playback:\n");
        showstat(phandle, frames_out);
        printf("Capture:\n");
        showstat(chandle, frames_in);
        showinmax(in_max);
        if (p_tstamp.tv_sec == p_tstamp.tv_sec && p_tstamp.tv_usec == c_tstamp.tv_usec)
            printf("Hardware sync\n");
        snd_pcm_drop(chandle);
        snd_pcm_nonblock(phandle, 0);
        snd_pcm_drain(phandle);
        snd_pcm_nonblock(phandle, !block ? 1 : 0);
#ifdef __ADD_DEVICE_B__
        snd_pcm_drop(chandle_b);
        snd_pcm_nonblock(phandle_b, 0);
        snd_pcm_drain(phandle_b);
        snd_pcm_nonblock(phandle_b, !block ? 1 : 0);
#endif
        if (ok) {
#if 1
            printf("Playback time = %li.%i, Record time = %li.%i, diff = %li\n",
               p_tstamp.tv_sec, (int)p_tstamp.tv_usec,
               c_tstamp.tv_sec, (int)c_tstamp.tv_usec,
               timediff(p_tstamp, c_tstamp));
#endif
            break;
        }

        free_pcm_handle(phandle, chandle, phandle_b, chandle_b);
    }

    free_pcm_buffer(buffer, buffer_b);
    close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
    return 0;
}
#endif /* __NO_MAIN__ */


/*******************************************************************************
 **
 ** Function         alsa_sleep_ms
 **
 ** Description      just milliseconds sleep function
 **
 ** Parameter        
 **
 ** Returns          
 *******************************************************************************/
void alsa_sleep_ms(int milliseconds) { // cross-platform sleep function
#ifdef WIN32
    Sleep(milliseconds);
#elif _POSIX_C_SOURCE >= 199309L
    struct timespec ts;
    ts.tv_sec = milliseconds / 1000;
    ts.tv_nsec = (milliseconds % 1000) * 1000000;
    nanosleep(&ts, NULL);
#else
    usleep(milliseconds * 1000);
#endif
}

/*******************************************************************************
 **
 ** Function         alsa_link_capture_playback
 **
 ** Description      link(connect) device("hw:0,2", "hw:0,0")
 **                  (*) blocking funciton, so call this function on thread or run-loop
 **
 ** Parameter        [IN] _stop_flag    - woking stop flag
 **                  [IN] playback_rate - 16000 or 8000
 **
 ** Returns          0 if successful execution, error code else
 *******************************************************************************/
int alsa_link_capture_playback(tEX_ALSA_GOSTOP *_stop_flag, int playback_rate) {
    snd_pcm_t *phandle = NULL;
    snd_pcm_t *chandle = NULL;
    snd_pcm_t *phandle_b = NULL;
    snd_pcm_t *chandle_b = NULL;

    char *buffer_b = NULL;
    char *buffer   = NULL;
    int err, latency, morehelp;
    int ok, rtn_err;
    snd_timestamp_t p_tstamp, c_tstamp;
    ssize_t r;
    size_t frames_in, frames_out, in_max;

    printf("step 00 [in]\n");
    if((_stop_flag == NULL) || (*_stop_flag == EX_ALSA_STOP)) {
        printf("[error] _stop_flag(%d) !!\n", *_stop_flag);
        return -1;
    }

    if(playback_rate == 8000 || playback_rate || 16000) {
        rate = playback_rate;
    } else {
        printf("[error] playback_rate(%d) !!\n", playback_rate);
        return -1;
    }
    dump_hw_params = 0x00;
    block          = 0x00;            /* block mode */
    use_poll       = 0x00;
    //latency    = latency_min;

    buffer     = malloc((latency_max * snd_pcm_format_width(format) / 8) * 2);
    if(buffer == NULL) {
        printf("[error] no memory !!\n");
        return -1;
    }
    
    buffer_b   = malloc((latency_max * snd_pcm_format_width(format) / 8) * 2);
    if(buffer_b == NULL) {
        free_pcm_buffer(buffer, buffer_b);
        printf("[error] no memory[B] !!\n");
        return -1;
    }


    printf("Playback device is %s\n", pdevice);
    printf("Capture device is %s\n", cdevice);

    printf("[B]Playback device is %s\n", pdevice_b);
    printf("[B]Capture device is %s\n", cdevice_b);

    printf("Parameters are %iHz, %s, %i channels, %s mode\n", rate, snd_pcm_format_name(format), channels, block ? "blocking" : "non-blocking");


    printf("step 01 [open]\n");
    // open
    if ((err = snd_pcm_open(&phandle, pdevice, SND_PCM_STREAM_PLAYBACK, block ? 0 : SND_PCM_NONBLOCK)) < 0) {
        printf("Playback open error: %s\n", snd_strerror(err));
        free_pcm_buffer(buffer, buffer_b);
        close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
        return err;
    }
    if ((err = snd_pcm_open(&chandle, cdevice, SND_PCM_STREAM_CAPTURE, block ? 0 : SND_PCM_NONBLOCK)) < 0) {
        printf("Record open error: %s\n", snd_strerror(err));
        free_pcm_buffer(buffer, buffer_b);
        close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
        return err;
    }

    if ((err = snd_pcm_open(&phandle_b, pdevice_b, SND_PCM_STREAM_PLAYBACK, block ? 0 : SND_PCM_NONBLOCK)) < 0) {
        printf("Playback(B) open error: %s\n", snd_strerror(err));
        free_pcm_buffer(buffer, buffer_b);
        close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
        return err;
    }
    if ((err = snd_pcm_open(&chandle_b, cdevice_b, SND_PCM_STREAM_CAPTURE, block ? 0 : SND_PCM_NONBLOCK)) < 0) {
        printf("Record(B) open error: %s\n", snd_strerror(err));
        free_pcm_buffer(buffer, buffer_b);
        close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
        return err;
    }
    
    // init
    printf("step 02 [init]\n");
    frames_in = frames_out = 0;
    latency    = latency_min;
    if ((rtn_err = setparams(phandle, chandle, &latency)) < 0) {
        printf("[error] setparams\n");

        if(rtn_err == EX_LATENCY_RTN_EXIT) {
            free_pcm_buffer(buffer, buffer_b);
            close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
            return rtn_err;
        }
    }

    latency    = latency_min;
    if ((rtn_err = setparams(phandle_b, chandle_b, &latency)) < 0) {
        printf("[error] setparams[B]\n");

        if(rtn_err == EX_LATENCY_RTN_EXIT) {
            free_pcm_buffer(buffer, buffer_b);
            close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
            return rtn_err;
        }
    }

    if ((err = snd_pcm_link(chandle, phandle)) < 0) {
        printf("Streams link error: %s\n", snd_strerror(err));
        free_pcm_buffer(buffer, buffer_b);

        close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
        return err;
    }

    if ((err = snd_pcm_link(chandle_b, phandle_b)) < 0) {
        printf("Streams link error[B]: %s\n", snd_strerror(err));
        free_pcm_buffer(buffer, buffer_b);

        free_pcm_handle(phandle, chandle, phandle_b, chandle_b);
        close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
        return err;
    }

    if (snd_pcm_format_set_silence(format, buffer, latency*channels) < 0) {
        fprintf(stderr, "silence error\n");
        // break;
    }
    if (writebuf(phandle, buffer, latency, &frames_out) < 0) {
        fprintf(stderr, "write error[01]\n");
        // break;
    }
    if (writebuf(phandle, buffer, latency, &frames_out) < 0) {
        fprintf(stderr, "write error[02]\n");
        // break;
    }
    if ((err = snd_pcm_start(chandle)) < 0) {
        printf("Go error: %s\n", snd_strerror(err));
        free_pcm_buffer(buffer, buffer_b);
        free_pcm_handle(phandle, chandle, phandle_b, chandle_b);
        close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
        return err;
    }

    if (writebuf(phandle_b, buffer_b, latency, &frames_out) < 0) {
        fprintf(stderr, "write error[B][01]\n");
        // break;
    }
    if (writebuf(phandle_b, buffer_b, latency, &frames_out) < 0) {
        fprintf(stderr, "write error[B][02]\n");
        // break;
    }
    if ((err = snd_pcm_start(chandle_b)) < 0) {
        printf("Go error[B]: %s\n", snd_strerror(err));
        free_pcm_buffer(buffer, buffer_b);

        free_pcm_handle(phandle, chandle, phandle_b, chandle_b);
        close_pcm_handle(phandle, chandle, phandle_b, chandle_b);
        return err;
    }

    gettimestamp(phandle, &p_tstamp);
    gettimestamp(chandle, &c_tstamp);


    // run
    printf("step 03 [run]\n");
    while((*_stop_flag) == EX_ALSA_GO) {
        if (use_poll) {
            /* use poll to wait for next event */
            snd_pcm_wait(chandle, 1000);
        }
        if ((r = readbuf(chandle, buffer, latency, &frames_in, &in_max)) < 0)
            ok = 0;
        else {
            if (writebuf(phandle, buffer, r, &frames_out) < 0)
                ok = 0;
        }

        if ((r = readbuf(chandle_b, buffer_b, latency, &frames_in, &in_max)) < 0)
            ok = 0;
        else {
            if (writebuf(phandle_b, buffer_b, r, &frames_out) < 0)
                ok = 0;
        }
    }

    // de-init
    printf("step 04 [de-init]\n");
    snd_pcm_drop(chandle);
    snd_pcm_nonblock(phandle, 0);
    snd_pcm_drain(phandle);
    snd_pcm_nonblock(phandle, !block ? 1 : 0);

    snd_pcm_drop(chandle_b);
    snd_pcm_nonblock(phandle_b, 0);
    snd_pcm_drain(phandle_b);
    snd_pcm_nonblock(phandle_b, !block ? 1 : 0);


    // close
    printf("step 05 [close]\n");
    free_pcm_handle(phandle, chandle, phandle_b, chandle_b);

    free_pcm_buffer(buffer, buffer_b);
    close_pcm_handle(phandle, chandle, phandle_b, chandle_b);

    printf("step 06 [out]\n");
    return 0;
}

