/*****************************************************************************
 **
 **  Name:           app_alsa.h
 **
 **  Description:    Linux ALSA utility functions (playback/recorde)
 **
 **  Copyright (c) 2011, Broadcom Corp., All Rights Reserved.
 **  Broadcom Bluetooth Core. Proprietary and confidential.
 **
 *****************************************************************************/

#ifndef APP_ALSA_H
#define APP_ALSA_H

#ifdef ADANIS_ALSA_WRAPPER
    #include <alsa/asoundlib.h>
#endif

#define APP_ALSA_PCM_FORMAT_S16_LE  0   /* Signed 16 Bits Little Endian */
#define APP_ALSA_PCM_FORMAT_S16_BE  1   /* Signed 16 Bits Big Endian */
#define APP_ALSA_PCM_FORMAT_U8 2

typedef UINT8 tAPP_ALSA_PCM_FORMAT;

#define APP_ALSA_PCM_ACCESS_RW_INTERLEAVED  0   /* RW Interleaved access */
typedef UINT8 tAPP_ALSA_PCM_ACCESS;

typedef struct
{
    BOOLEAN blocking;   /* Blocking mode */
    tAPP_ALSA_PCM_FORMAT format; /* PCM Format */
    tAPP_ALSA_PCM_ACCESS access; /* PCM access */
    BOOLEAN stereo; /* Nono or Stereo */
    BOOLEAN skip_snd_pcm_set_params; /* 1: skip snd_pcm_set_params, 0:using snd_pcm_set_params */
    UINT32 sample_rate; /* Sampling rate (e.g. 8000 for 8KHz) */
    UINT32 latency; /* Latency (ms) */
} tAPP_ALSA_PLAYBACK_OPEN;

typedef tAPP_ALSA_PLAYBACK_OPEN tAPP_ALSA_CAPTURE_OPEN;

#ifdef ADANIS_ALSA_WRAPPER
    typedef struct {
        BOOLEAN                 sco_using_I2S_playback;
        BOOLEAN                 sco_using_I2S_capture;
        tBTA_SERVICE_MASK       services;
        
        snd_pcm_t              *p_playback_handle;
        snd_pcm_t              *p_capture_handle;

        snd_pcm_t              *p_playback_handle_I2S; /* SCO I2S */
        snd_pcm_t              *p_capture_handle_I2S;  /* SCO I2S */
        
        tAPP_ALSA_PLAYBACK_OPEN playback_param;
        tAPP_ALSA_CAPTURE_OPEN  capture_param;
    } tADANIS_ALSA_CB;
#endif

/*******************************************************************************
 **
 ** Function        app_alsa_init
 **
 ** Description     ALSA initialization
 **
 ** Parameters      None
 **
 ** Returns         status
 **
 *******************************************************************************/
int app_alsa_init(void);

/*******************************************************************************
 **
 ** Function        app_alsa_playback_open_init
 **
 ** Description     Init ALSA playback parameters (to Speaker)
 **
 ** Parameters      p_open: Open parameters
 **
 ** Returns         status
 **
 *******************************************************************************/
int app_alsa_playback_open_init(tAPP_ALSA_PLAYBACK_OPEN *p_open);

/*******************************************************************************
 **
 ** Function        app_alsa_playback_open
 **
 ** Description     Open ALSA playback channel (to Speaker)
 **
 ** Parameters      p_open: Open parameters
 **
 ** Returns         status
 **
 *******************************************************************************/
int app_alsa_playback_open(tAPP_ALSA_PLAYBACK_OPEN *p_open);

/*******************************************************************************
 **
 ** Function        app_alsa_playback_close
 **
 ** Description     close ALSA playback channel (to Speaker)
 **
 ** Parameters      none
 **
 ** Returns         status
 **
 *******************************************************************************/
int app_alsa_playback_close(void);

/*******************************************************************************
 **
 ** Function        app_alsa_playback_write
 **
 ** Description     Write PCM sample to Playback Channel
 **
 ** Parameters      p_buffer: pointer on buffer containing
 **                 buffer_size: Buffer size
 **
 ** Returns         number of bytes written (-1 if error)
 **
 *******************************************************************************/
int app_alsa_playback_write(void *p_buffer, int buffer_size);

/*******************************************************************************
 **
 ** Function        app_alsa_capture_open_init
 **
 ** Description     Init ALSA Capture parameters (from microphone)
 **
 ** Parameters      p_open: Capture parameters
 **
 ** Returns         status
 **
 *******************************************************************************/
int app_alsa_capture_open_init(tAPP_ALSA_CAPTURE_OPEN *p_open);

/*******************************************************************************
 **
 ** Function        app_alsa_capture_open
 **
 ** Description     Open ALSA Capture channel (from microphone)
 **
 ** Parameters      p_open: Capture parameters
 **
 ** Returns         status
 **
 *******************************************************************************/
int app_alsa_capture_open(tAPP_ALSA_CAPTURE_OPEN *p_open);

#ifdef APP_AV_BCST_PLAY_LOOPDRV_INCLUDED
/*******************************************************************************
 **
 ** Function        app_alsa_capture_loopback_open
 **
 ** Description     Open ALSA Capture channel (from loopback driver)
 **
 ** Parameters      p_open: Capture parameters
 **
 ** Returns         status
 **
 *******************************************************************************/
int app_alsa_capture_loopback_open(tAPP_ALSA_CAPTURE_OPEN *p_open);
#endif

/*******************************************************************************
 **
 ** Function        app_alsa_capture_close
 **
 ** Description     close ALSA Capture channel (from Microphone)
 **
 ** Parameters      none
 **
 ** Returns         status
 **
 *******************************************************************************/
int app_alsa_capture_close(void);

/*******************************************************************************
 **
 ** Function        app_alsa_capture_read
 **
 ** Description     Read PCM samples from Capture Channel
 **
 ** Parameters      p_buffer: pointer on buffer containing
 **                 buffer_size: size of the buffer
 **
 ** Returns         number of bytes read (< 0 if error)
 **
 *******************************************************************************/
int app_alsa_capture_read(void *p_buffer, int buffer_size);

#ifdef ADANIS_ALSA_WRAPPER
/*******************************************************************************
 **
 ** Function        adanis_alsa_init
 **
 ** Description     ALSA initialization
 **
 ** Parameters      None
 **
 ** Returns         status
 **
 *******************************************************************************/
int adanis_alsa_init(void);

/*******************************************************************************
 **
 ** Function        adanis_alsa_get_cb_handle
 **
 ** Description     get tADANIS_ALSA_CB's handle
 **
 ** Parameters      None
 **
 ** Returns         tADANIS_ALSA_CB*
 **
 *******************************************************************************/
tADANIS_ALSA_CB* adanis_alsa_get_cb_handle(void);

/*******************************************************************************
 **
 ** Function        adanis_alsa_mutex_lock
 **
 ** Description     mutex lock
 **
 ** Parameters      None
 **
 ** Returns         None
 **
 *******************************************************************************/
void adanis_alsa_mutex_lock(void);

/*******************************************************************************
 **
 ** Function        adanis_alsa_mutex_unlock
 **
 ** Description     mutex unlock
 **
 ** Parameters      None
 **
 ** Returns         None
 **
 *******************************************************************************/
void adanis_alsa_mutex_unlock(void);

/*******************************************************************************
 **
 ** Function        adanis_alsa_playback_open_init
 **
 ** Description     Init ALSA playback parameters (to Speaker)
 **
 ** Parameters      p_open: Open parameters
 **
 ** Returns         status
 **
 *******************************************************************************/
int adanis_alsa_playback_open_init(tAPP_ALSA_PLAYBACK_OPEN *p_open, tBTA_SERVICE_MASK services);

/*******************************************************************************
 **
 ** Function        adanis_alsa_capture_open_init
 **
 ** Description     Init ALSA Capture parameters (from microphone)
 **
 ** Parameters      p_open: Capture parameters
 **
 ** Returns         status
 **
 *******************************************************************************/
int adanis_alsa_capture_open_init(tAPP_ALSA_CAPTURE_OPEN *p_open);

/*******************************************************************************
 **
 ** Function        adanis_alsa_capture_close
 **
 ** Description     close ALSA Capture channel (from Microphone)
 **
 ** Parameters      none
 **
 ** Returns         status
 **
 *******************************************************************************/
int adanis_alsa_capture_close(void);

/*******************************************************************************
 **
 ** Function        app_alsa_capture_open
 **
 ** Description     Open ALSA Capture channel (from microphone)
 **
 ** Parameters      p_open: Capture parameters
 **
 ** Returns         snd_pcm_t* 
 **
 *******************************************************************************/
snd_pcm_t* adanis_alsa_capture_open(tAPP_ALSA_CAPTURE_OPEN *p_open, BOOLEAN is_SCO_using_I2S);

/*******************************************************************************
 **
 ** Function        adanis_alsa_capture_handle
 **
 ** Description     get ALSA capture handle
 **
 ** Parameters      tBTA_SERVICE_MASK
 **
 ** Returns         *snd_pcm_t
 **
 *******************************************************************************/
snd_pcm_t* adanis_alsa_capture_handle(tBTA_SERVICE_MASK check_services);

/*******************************************************************************
 **
 ** Function        adanis_alsa_playback_handle
 **
 ** Description     get ALSA playback handle
 **
 ** Parameters      tBTA_SERVICE_MASK
 **
 ** Returns         *snd_pcm_t
 **
 *******************************************************************************/
snd_pcm_t* adanis_alsa_playback_handle(tBTA_SERVICE_MASK check_services);

/*******************************************************************************
 **
 ** Function        adanis_alsa_playback_status
 **
 ** Description     check ALSA playback status
 **
 ** Parameters      none
 **
 ** Returns         tBTA_SERVICE_MASK
 **
 *******************************************************************************/
tBTA_SERVICE_MASK adanis_alsa_playback_status(void);

/*******************************************************************************
 **
 ** Function        adanis_alsa_playback_close
 **
 ** Description     close ALSA playback channel (to Speaker)
 **
 ** Parameters      none
 **
 ** Returns         status
 **
 *******************************************************************************/
int adanis_alsa_playback_close(tBTA_SERVICE_MASK services);

/*******************************************************************************
 **
 ** Function        adanis_alsa_playback_open
 **
 ** Description     Open ALSA playback channel (to Speaker)
 **
 ** Parameters      p_open: Open parameters
 **
 ** Returns         snd_pcm_t* 
 **
 *******************************************************************************/
snd_pcm_t* adanis_alsa_playback_open(tAPP_ALSA_PLAYBACK_OPEN *p_open, tBTA_SERVICE_MASK services, BOOLEAN is_SCO_using_I2S);

/*******************************************************************************
 **
 ** Function        adanis_alsa_writebuf
 **
 ** Description     ALSA write buffer data
 **
 ** Parameters      None
 **
 ** Returns         long
 **
 *******************************************************************************/
long adanis_alsa_writebuf(snd_pcm_t *handle, char *buf, long len, size_t *frames);
#endif


#endif
