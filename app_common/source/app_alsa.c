/*****************************************************************************
 **
 **  Name:           app_alsa.c
 **
 **  Description:    Linux ALSA utility functions (playback/capture)
 **
 **  Copyright (c) 2011, Broadcom Corp., All Rights Reserved.
 **  Broadcom Bluetooth Core. Proprietary and confidential.
 **
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#ifdef ADANIS_ALSA_WRAPPER
    #include <pthread.h>
#else
    #include <alsa/asoundlib.h>
#endif

#include "app_utils.h"

#include "bsa_api.h"

#include "app_alsa.h"
/*
 * Definitions
 */

#ifndef BSA_ALSA_DEV_NAME
    /*
     * TODO (TARGET)
     * "hw:0,0" 사용 시 snd_pcm_set_params 함수에서 channel이 1일 경우, 아래와 같은 오류가 발생함.
     * => ALSA lib ../../../alsa-lib-1.1.0/src/pcm/pcm.c:8044:(snd_pcm_set_params) Channels count (1) not available for PLAYBACK: Invalid argument
     */
    #define BSA_ALSA_DEV_NAME "default" // "default" // "hw:0,0"
#endif

#define BSA_ALSA_DEV_NAME_HW_0_0 "hw:0,0" // "default" // "hw:0,0"

#ifdef ADANIS_ALSA_WRAPPER
    /*
     * TODO
     * I2S로 open할 경우,
     *      snd_pcm_open -> snd_pcm_set_params -> snd_pcm 설정 함수 사용시 error 발생함.
     *  => 정리 필요함.
     */
    #define BSA_I2S_DEVICE_NAME "hw:0,2"
#endif

static char *alsa_device = BSA_ALSA_DEV_NAME; /* ALSA default device */

#ifdef APP_AV_BCST_PLAY_LOOPDRV_INCLUDED
static char *alsa_device_loopback = "loop"; /* ALSA loopback device */
#endif

typedef struct
{
    snd_pcm_t *p_playback_handle;
    tAPP_ALSA_PLAYBACK_OPEN playback_param;
    snd_pcm_t *p_capture_handle;
    tAPP_ALSA_CAPTURE_OPEN capture_param;
} tAPP_ALSA_CB;

/*
 * Global variables
 */
tAPP_ALSA_CB app_alsa_cb;

/*
 * Local functions
 */
static snd_pcm_format_t app_alsa_get_pcm_format(tAPP_ALSA_PCM_FORMAT app_format);

#ifdef ADANIS_ALSA_WRAPPER
    /* TODO DEP TARGET */
static char *alsa_device_HW_0_0 = BSA_ALSA_DEV_NAME_HW_0_0; /* ALSA default device */
static char *alsa_device_I2S_BT = BSA_I2S_DEVICE_NAME; /* ALSA default device */

static tADANIS_ALSA_CB adanis_alsa_cb    = {0,};
static pthread_mutex_t adanis_alsa_mutex = PTHREAD_MUTEX_INITIALIZER;
static int             adanis_alsa_init_flag = 0x00;

#define MASK_HFP_HSP (BTA_HSP_HS_SERVICE_MASK|BTA_HFP_HS_SERVICE_MASK|BTA_HSP_SERVICE_MASK|BTA_HFP_SERVICE_MASK)
#define MASK_A2DP    (BTA_A2DP_SRC_SERVICE_MASK|BTA_A2DP_SERVICE_MASK)
/*******************************************************************************
 **
 ** Function        adanis_alsa_init
 **
 ** Description     ALSA initialization
 **
 ** Parameters      None
 **
 ** Returns         status
 **
 *******************************************************************************/
int adanis_alsa_init(void) {
    APP_DEBUG0("adanis_alsa_init");

    if(!adanis_alsa_init_flag) {
        adanis_alsa_init_flag = 0x01;
        memset (&adanis_alsa_cb, 0x00, sizeof(adanis_alsa_cb));
    }
    return TRUE;
}


/*******************************************************************************
 **
 ** Function        adanis_alsa_get_cb_handle
 **
 ** Description     get tADANIS_ALSA_CB's handle
 **
 ** Parameters      None
 **
 ** Returns         tADANIS_ALSA_CB*
 **
 *******************************************************************************/
tADANIS_ALSA_CB* adanis_alsa_get_cb_handle(void) {
    APP_DEBUG0("adanis_alsa_get_cb_handle");

    return &adanis_alsa_cb;
}


/*******************************************************************************
 **
 ** Function        adanis_alsa_mutex_lock
 **
 ** Description     mutex lock
 **
 ** Parameters      None
 **
 ** Returns         None
 **
 *******************************************************************************/
void adanis_alsa_mutex_lock(void) {
    //APP_DEBUG0("adanis_alsa_mutex_lock");
    pthread_mutex_lock(&adanis_alsa_mutex);
}


/*******************************************************************************
 **
 ** Function        adanis_alsa_mutex_unlock
 **
 ** Description     mutex unlock
 **
 ** Parameters      None
 **
 ** Returns         None
 **
 *******************************************************************************/
void adanis_alsa_mutex_unlock(void) {
    pthread_mutex_unlock(&adanis_alsa_mutex);
    //APP_DEBUG0("adanis_alsa_mutex_unlock");
}

/*******************************************************************************
 **
 ** Function        adanis_alsa_playback_open_init
 **
 ** Description     Init ALSA playback parameters (to Speaker)
 **
 ** Parameters      p_open: Open parameters
 **
 ** Returns         status
 **
 *******************************************************************************/
int adanis_alsa_playback_open_init(tAPP_ALSA_PLAYBACK_OPEN *p_open, tBTA_SERVICE_MASK services)
{
    p_open->access      = APP_ALSA_PCM_ACCESS_RW_INTERLEAVED;
    p_open->blocking    = FALSE; /* Non Blocking */
    p_open->format      = APP_ALSA_PCM_FORMAT_S16_LE; /* Signed 16 bits Little Endian*/
    p_open->skip_snd_pcm_set_params = 0x00;

    if(services&MASK_A2DP) {
        p_open->sample_rate = 44100;  /* 44.1KHz */
        p_open->stereo      = TRUE;   /* Stereo */
        p_open->latency     = 500000; /* 500ms */
    } else {
        p_open->sample_rate = 8000;  /* 8 KHz */
        p_open->stereo      = FALSE;  /* Mono */
        p_open->latency     = 100000; /* 100ms */
    }

    return 0;
}

/*******************************************************************************
 **
 ** Function        adanis_alsa_capture_open_init
 **
 ** Description     Init ALSA Capture parameters (from microphone)
 **
 ** Parameters      p_open: Capture parameters
 **
 ** Returns         status
 **
 *******************************************************************************/
int adanis_alsa_capture_open_init(tAPP_ALSA_CAPTURE_OPEN *p_open)
{
    memset(p_open, 0x00, sizeof(tAPP_ALSA_CAPTURE_OPEN));

    return 0;
}

/*******************************************************************************
 **
 ** Function        adanis_alsa_capture_close
 **
 ** Description     close ALSA Capture channel (from Microphone)
 **
 ** Parameters      none
 **
 ** Returns         status
 **
 *******************************************************************************/
int adanis_alsa_capture_close(void) {
    int status;

    if ((adanis_alsa_cb.p_capture_handle == NULL) && (adanis_alsa_cb.p_capture_handle_I2S == NULL)) {
        APP_ERROR0("Capture channel was not opened");
        return -1;
    }
    APP_DEBUG0("ALSA driver: Closing Capture mode");

    status = snd_pcm_close(adanis_alsa_cb.p_capture_handle);
    if (status < 0x00) {
        APP_ERROR1("snd_pcm_close error status:%d", status);
    } else {
        adanis_alsa_cb.p_capture_handle = NULL;
    }

    if(adanis_alsa_cb.p_capture_handle_I2S) {
        status = snd_pcm_close(adanis_alsa_cb.p_capture_handle_I2S);
        if (status < 0x00) {
            APP_ERROR1("snd_pcm_close error status[B]:%d", status);
        } else {
            adanis_alsa_cb.p_capture_handle_I2S = NULL;
        }
    }
    return status;
}

/*******************************************************************************
 **
 ** Function        app_alsa_capture_open
 **
 ** Description     Open ALSA Capture channel (from microphone)
 **
 ** Parameters      p_open: Capture parameters
 **
 ** Returns         snd_pcm_t *
 **
 *******************************************************************************/
snd_pcm_t* adanis_alsa_capture_open(tAPP_ALSA_CAPTURE_OPEN *p_open, BOOLEAN using_I2S/*SCO only*/) {
    int mode = 0x00; /* Default is blocking */
    unsigned int nb_channels;
    char *alsa_device_name = alsa_device;
    int rv;
    snd_pcm_format_t format;
    snd_pcm_access_t access;

    //if(using_I2S == TRUE) {
    //    alsa_device_name = alsa_device_HW_0_0;
    //}

    APP_DEBUG1("adanis audio driver Capture([%s|%s], using_I2S:%d)", alsa_device_name, alsa_device_I2S_BT, using_I2S);

    /* Sanity check if already opened */
    if (adanis_alsa_cb.p_capture_handle != NULL) {
        APP_DEBUG0("Capture was already opened");
    }

    /* check PCM Format parameter */
    format = app_alsa_get_pcm_format(p_open->format);
    if (format == SND_PCM_FORMAT_UNKNOWN) {
        return NULL;
    }

    /* Check PCM access parameter */
    if (p_open->access == APP_ALSA_PCM_ACCESS_RW_INTERLEAVED) {
        access = SND_PCM_ACCESS_RW_INTERLEAVED;
    } else {
        APP_ERROR1("Unsupported PCM access:%d", p_open->access);
        return NULL;
    }
    /* Check Blocking parameter */
    if (p_open->blocking == FALSE) {
        mode = SND_PCM_NONBLOCK;
    }

    /* check Stereo parameter */
    if (p_open->stereo) {
        nb_channels = 0x02;
    } else {
        nb_channels = 0x01;
    }

#if 1
    printf("Parameters[capture] are %iHz, %s, %i channels, mode:%d\n", p_open->sample_rate, snd_pcm_format_name(format), nb_channels, mode);
#endif

    /* Save the Capture open parameters */
    memcpy(&adanis_alsa_cb.capture_param, p_open, sizeof(adanis_alsa_cb.capture_param));

    /* Open ALSA driver */
    rv = snd_pcm_open(&adanis_alsa_cb.p_capture_handle, alsa_device_name, 
        SND_PCM_STREAM_CAPTURE, mode);
    if (rv < 0x00) {
        APP_ERROR1("unable to open ALSA device in Capture mode:%s", snd_strerror(rv));
        return NULL;
    }
    APP_DEBUG0("ALSA driver opened in Capture mode");

    //if(using_I2S == FALSE) {
        /* Configure ALSA driver with PCM parameters */
        rv = snd_pcm_set_params(adanis_alsa_cb.p_capture_handle,
                format, access, nb_channels, p_open->sample_rate,
                0x01, /* SW resample */ p_open->latency);
        if (rv) {
            APP_ERROR1("Unable to config ALSA device:%s", snd_strerror(rv));
            snd_pcm_close(adanis_alsa_cb.p_capture_handle);
            adanis_alsa_cb.p_capture_handle = NULL;
            return NULL;
        }
    //}

    if(using_I2S == TRUE) {
        /* Open ALSA driver */
        rv = snd_pcm_open(&adanis_alsa_cb.p_capture_handle_I2S, alsa_device_I2S_BT, 
            SND_PCM_STREAM_CAPTURE, mode);
        if (rv < 0x00) {
            APP_ERROR1("unable to open ALSA device in Capture[B] mode:%s", snd_strerror(rv));
            
            snd_pcm_close(adanis_alsa_cb.p_capture_handle);
            adanis_alsa_cb.p_capture_handle = NULL;
            return NULL;
        }
        APP_DEBUG0("ALSA driver opened in Capture mode[B]");

        /* Configure ALSA driver with PCM parameters */
        rv = snd_pcm_set_params(adanis_alsa_cb.p_capture_handle_I2S,
                format, access, nb_channels, p_open->sample_rate,
                0x01, /* SW resample */ p_open->latency);

        if (rv) {
            APP_ERROR1("Unable to config ALSA device[B]:%s", snd_strerror(rv));
            
            snd_pcm_close(adanis_alsa_cb.p_capture_handle);
            adanis_alsa_cb.p_capture_handle = NULL;
            
            snd_pcm_close(adanis_alsa_cb.p_capture_handle_I2S);
            adanis_alsa_cb.p_capture_handle_I2S = NULL;
            return NULL;
        }

        adanis_alsa_cb.sco_using_I2S_capture = TRUE;
    }

    return adanis_alsa_cb.p_capture_handle;
}

/*******************************************************************************
 **
 ** Function        adanis_alsa_capture_handle
 **
 ** Description     get ALSA capture handle
 **
 ** Parameters      tBTA_SERVICE_MASK
 **
 ** Returns         *snd_pcm_t
 **
 *******************************************************************************/
snd_pcm_t* adanis_alsa_capture_handle(tBTA_SERVICE_MASK check_services) {
    if((adanis_alsa_cb.services&check_services) && adanis_alsa_cb.p_capture_handle)
        return adanis_alsa_cb.p_capture_handle;
    else
        return NULL;
}


/*******************************************************************************
 **
 ** Function        adanis_alsa_playback_handle
 **
 ** Description     get ALSA playback handle
 **
 ** Parameters      tBTA_SERVICE_MASK
 **
 ** Returns         *snd_pcm_t
 **
 *******************************************************************************/
snd_pcm_t* adanis_alsa_playback_handle(tBTA_SERVICE_MASK check_services) {
    if((adanis_alsa_cb.services&check_services) && adanis_alsa_cb.p_playback_handle)
        return adanis_alsa_cb.p_playback_handle;
    else
        return NULL;
}

/*******************************************************************************
 **
 ** Function        adanis_alsa_playback_status
 **
 ** Description     check ALSA playback status
 **
 ** Parameters      none
 **
 ** Returns         tBTA_SERVICE_MASK
 **
 *******************************************************************************/
tBTA_SERVICE_MASK adanis_alsa_playback_status(void) {
    if (adanis_alsa_cb.p_playback_handle == NULL) {
        APP_DEBUG0("Playback services IDLE");
        return 0x00;
    }

/*
    APP_DEBUG1("Playback services 0x%X", adanis_alsa_cb.services);
*/
    return adanis_alsa_cb.services;
}


/*******************************************************************************
 **
 ** Function        adanis_alsa_playback_close
 **
 ** Description     close ALSA playback channel (to Speaker)
 **
 ** Parameters      tBTA_SERVICE_MASK
 **
 ** Returns         status
 **
 *******************************************************************************/
int adanis_alsa_playback_close(tBTA_SERVICE_MASK closing_services) {
    int status;
    tBTA_SERVICE_MASK old_services = adanis_alsa_cb.services;
    snd_pcm_t        *p_handle     = adanis_alsa_cb.p_playback_handle;
    snd_pcm_t        *p_handle_I2S = adanis_alsa_cb.p_playback_handle_I2S;

    if ((p_handle == NULL) && (p_handle_I2S == NULL)) {
        APP_ERROR0("Playback channel was not opened");
        return -1;
    }

    if((old_services&closing_services) == 0x00) { /* no matching service */
        APP_ERROR1("Playback channel(0x%X) is not opened", closing_services);
        return -1;
    }

    adanis_alsa_cb.services = 0x00;
    adanis_alsa_cb.p_playback_handle     = NULL;
    adanis_alsa_cb.p_playback_handle_I2S = NULL;
    
    APP_DEBUG1("ALSA driver: Closing Playback(0x%X)", closing_services);

    adanis_alsa_mutex_lock();

    status = snd_pcm_close(p_handle);
    if (status < 0x00) {
        APP_ERROR1("snd_pcm_close error status:%d", status);

        // re-try
        status = snd_pcm_close(p_handle);
        if (status < 0x00) {
            APP_ERROR1("snd_pcm_close 2nd error status:%d", status);
        } else {
            APP_DEBUG0("ALSA driver: Close Playback");
        }
    }

    if(p_handle_I2S) {
        status = snd_pcm_close(p_handle_I2S);
        if (status < 0x00) {
            APP_ERROR1("snd_pcm_close error status[B]:%d", status);

            // re-try
            status = snd_pcm_close(p_handle_I2S);
            if (status < 0x00) {
                APP_ERROR1("snd_pcm_close 2nd error status[B]:%d", status);
            } else {
                APP_DEBUG0("ALSA driver: Close Playback[B]");
            }
        }
    }
    adanis_alsa_mutex_unlock();

    APP_DEBUG1("ALSA driver: Close status(%d) Playback(0x%X)", status, closing_services);
    return status;
}

/*******************************************************************************
 **
 ** Function        adanis_alsa_playback_open
 **
 ** Description     Open ALSA playback channel (to Speaker)
 **
 ** Parameters      p_open: Open parameters
 **
 ** Returns         snd_pcm_t* 
 **
 *******************************************************************************/
snd_pcm_t* adanis_alsa_playback_open(tAPP_ALSA_PLAYBACK_OPEN *p_open, tBTA_SERVICE_MASK opening_services, BOOLEAN using_I2S/*SCO only*/)
{
    int mode = 0x00; /* Default if Blocking */
    int status;
    unsigned int nb_channels;
    int rv;
    snd_pcm_format_t format;
    snd_pcm_access_t access;
    tBTA_SERVICE_MASK old_services = adanis_alsa_cb.services;
    char *alsa_device_name = alsa_device;

    //if(using_I2S == TRUE) {
    //    alsa_device_name = alsa_device_HW_0_0;
    //}

    APP_DEBUG1("adanis audio driver Playback([%s|%s] open:0x%X, old:0x%X, using_I2S:%d)", alsa_device_name, alsa_device_I2S_BT, opening_services, old_services, using_I2S);
    // check service for closing alsa
    if((adanis_alsa_cb.p_playback_handle) && 
       (old_services) && 
       (old_services != opening_services)) {
        // close other
        status = adanis_alsa_playback_close(old_services);
        if(status < 0x00) {
            // re-try
            adanis_alsa_playback_close(old_services);
        }
    }

    adanis_alsa_cb.services = 0x00;    
    /* check if already opened */
    if (adanis_alsa_cb.p_playback_handle != NULL) {
        APP_ERROR0("Playback was already opened");
        //return -1;
        return NULL;
    }

    /* check PCM Format parameter */
    format = app_alsa_get_pcm_format(p_open->format);
    if (format == SND_PCM_FORMAT_UNKNOWN) {
        //return -1;
        return NULL;
    }

    /* Check PCM access parameter */
    if (p_open->access == APP_ALSA_PCM_ACCESS_RW_INTERLEAVED) {
        access = SND_PCM_ACCESS_RW_INTERLEAVED;
    } else {
        APP_ERROR1("Unsupported PCM access:%d", p_open->access);
        //return -1;
        return NULL;
    }
    
    /* Check Blocking parameter #define SND_PCM_NONBLOCK 0x0001 */
    if (p_open->blocking == FALSE) {
        mode = SND_PCM_NONBLOCK;
    }

    /* check Stereo parameter */
    if (p_open->stereo == TRUE) {
        nb_channels = 0x02;
    } else {
        nb_channels = 0x01;
    }

#if 1
    printf("Parameters[playback] are %iHz, %s, %i channels, mode:%d, latency:%d\n", p_open->sample_rate, snd_pcm_format_name(format), nb_channels, mode, p_open->latency);
#endif

    /* Save the Playback open parameters */
    memcpy(&adanis_alsa_cb.playback_param, p_open, sizeof(adanis_alsa_cb.playback_param));

    adanis_alsa_mutex_lock();

    /* Open ALSA driver */
    rv = snd_pcm_open(&adanis_alsa_cb.p_playback_handle, alsa_device_name,
            SND_PCM_STREAM_PLAYBACK, mode);
    if (rv < 0x00) {
        APP_ERROR1("unable to open ALSA device in playback mode:%s", snd_strerror(rv));
        adanis_alsa_mutex_unlock();
        return NULL;
    }

    //if(using_I2S == FALSE) {
        /* Configure ALSA driver with PCM parameters */
        rv = snd_pcm_set_params(adanis_alsa_cb.p_playback_handle,
                format, access, nb_channels, p_open->sample_rate,
                0x01, /* SW resample */ p_open->latency);
        if (rv < 0x00) {
            APP_ERROR1("Unable to config ALSA device:%s", snd_strerror(rv));
            snd_pcm_close(adanis_alsa_cb.p_playback_handle);
            adanis_alsa_cb.p_playback_handle = NULL;
            adanis_alsa_mutex_unlock();
            return NULL;
        }
    //}

    if(using_I2S == TRUE) {
        /* Open ALSA driver */
        rv = snd_pcm_open(&adanis_alsa_cb.p_playback_handle_I2S, alsa_device_I2S_BT,
                SND_PCM_STREAM_PLAYBACK, mode);
        if (rv < 0x00) {
            APP_ERROR1("unable to open ALSA device in playback[B] mode:%s", snd_strerror(rv));
            
            snd_pcm_close(adanis_alsa_cb.p_playback_handle);
            adanis_alsa_cb.p_playback_handle = NULL;
            
            adanis_alsa_mutex_unlock();
            return NULL;
        }

        /* Configure ALSA driver with PCM parameters */
        rv = snd_pcm_set_params(adanis_alsa_cb.p_playback_handle_I2S,
                format, access, nb_channels, p_open->sample_rate,
                0x01, /* SW resample */ p_open->latency);

        if (rv < 0x00) {
            APP_ERROR1("Unable to config ALSA device[B]:%s", snd_strerror(rv));

            snd_pcm_close(adanis_alsa_cb.p_playback_handle);
            adanis_alsa_cb.p_playback_handle = NULL;
            
            snd_pcm_close(adanis_alsa_cb.p_playback_handle_I2S);
            adanis_alsa_cb.p_playback_handle_I2S = NULL;
            adanis_alsa_mutex_unlock();
            return NULL;
        }
        adanis_alsa_cb.sco_using_I2S_playback = TRUE;
    }

    APP_DEBUG0("ALSA driver opened in playback mode");

    // save service mask
    adanis_alsa_cb.services = opening_services;
    adanis_alsa_mutex_unlock();

    return adanis_alsa_cb.p_playback_handle;
}

/*******************************************************************************
 **
 ** Function        adanis_alsa_writebuf
 **
 ** Description     ALSA write buffer data
 **
 ** Parameters      None
 **
 ** Returns         long
 **
 *******************************************************************************/
long adanis_alsa_writebuf(snd_pcm_t *handle, char *buf, long len, size_t *frames) {
    long r = 0x00;
    while (len > 0) {
        r = snd_pcm_writei(handle, buf, len);
        if (r == -EAGAIN)
            continue;
        // printf("write = %li\n", r);
        if (r < 0)
                return r;
        buf += r * 4;
        len -= r;
        *frames += r;
    }
    return 0;
}
#endif

/*******************************************************************************
 **
 ** Function        app_alsa_init
 **
 ** Description     ALSA initialization
 **
 ** Parameters      None
 **
 ** Returns         status
 **
 *******************************************************************************/
int app_alsa_init(void)
{
    APP_DEBUG0("");
    memset (&app_alsa_cb, 0, sizeof(app_alsa_cb));
    return 0;
}

/*******************************************************************************
 **
 ** Function        app_alsa_playback_open_init
 **
 ** Description     Init ALSA playback parameters (to Speaker)
 **
 ** Parameters      p_open: Open parameters
 **
 ** Returns         status
 **
 *******************************************************************************/
int app_alsa_playback_open_init(tAPP_ALSA_PLAYBACK_OPEN *p_open)
{
    p_open->access = APP_ALSA_PCM_ACCESS_RW_INTERLEAVED;
    p_open->blocking = FALSE; /* Non Blocking */
    p_open->format = APP_ALSA_PCM_FORMAT_S16_LE; /* Signed 16 bits Little Endian*/
    p_open->sample_rate = 44100; /* 44.1KHz */
    p_open->stereo = TRUE; /* Stereo */
    p_open->latency = 200000; /* 200ms */

    return 0;
}

/*******************************************************************************
 **
 ** Function        app_alsa_playback_open
 **
 ** Description     Open ALSA playback channel (to Speaker)
 **
 ** Parameters      p_open: Open parameters
 **
 ** Returns         status
 **
 *******************************************************************************/
int app_alsa_playback_open(tAPP_ALSA_PLAYBACK_OPEN *p_open)
{
    int mode = 0; /* Default if Blocking */
    unsigned int nb_channels;
    int rv;
    snd_pcm_format_t format;
    snd_pcm_access_t access;

    APP_DEBUG0("Opening ALSA/Asound audio driver Playback");

    /* check if already opened */
    if (app_alsa_cb.p_playback_handle != NULL)
    {
        APP_ERROR0("Playback was already opened");
        return -1;
    }

    /* check PCM Format parameter */
    format = app_alsa_get_pcm_format(p_open->format);
    if (format == SND_PCM_FORMAT_UNKNOWN)
    {
        return -1;
    }

    /* Check PCM access parameter */
    if (p_open->access == APP_ALSA_PCM_ACCESS_RW_INTERLEAVED)
    {
        access = SND_PCM_ACCESS_RW_INTERLEAVED;
    }
    else
    {
        APP_ERROR1("Unsupported PCM access:%d", p_open->access);
        return -1;
    }
    /* Check Blocking parameter */
    if (p_open->blocking == FALSE)
    {
        mode = SND_PCM_NONBLOCK;
    }

    /* check Stereo parameter */
    if (p_open->stereo)
    {
        nb_channels = 2;
    }
    else
    {
        nb_channels = 1;
    }

    /* Save the Playback open parameters */
    memcpy(&app_alsa_cb.playback_param, p_open, sizeof(app_alsa_cb.playback_param));

    /* Open ALSA driver */
    rv = snd_pcm_open(&app_alsa_cb.p_playback_handle, alsa_device,
            SND_PCM_STREAM_PLAYBACK, mode);
    if (rv < 0)
    {
        APP_ERROR1("unable to open ALSA device in playback mode:%s", snd_strerror(rv));
        return rv;
    }

    /* Configure ALSA driver with PCM parameters */
    rv = snd_pcm_set_params(app_alsa_cb.p_playback_handle,
            format,
            access,
            nb_channels,
            p_open->sample_rate,
            1, /* SW resample */
            p_open->latency);
    if (rv < 0)
    {
        APP_ERROR1("Unable to config ALSA device:%s", snd_strerror(rv));
        snd_pcm_close(app_alsa_cb.p_playback_handle);
        app_alsa_cb.p_playback_handle = NULL;
        return rv;
    }

    APP_DEBUG0("ALSA driver opened in playback mode");

    return 0;
}

/*******************************************************************************
 **
 ** Function        app_alsa_playback_close
 **
 ** Description     close ALSA playback channel (to Speaker)
 **
 ** Parameters      none
 **
 ** Returns         status
 **
 *******************************************************************************/
int app_alsa_playback_close(void)
{
    int status;

    if (app_alsa_cb.p_playback_handle == NULL)
    {
        APP_ERROR0("Playback channel was not opened");
        return -1;
    }
    APP_DEBUG0("ALSA driver: Closing Playback");

    status = snd_pcm_close(app_alsa_cb.p_playback_handle);
    if (status < 0)
    {
        APP_ERROR1("snd_pcm_close error status:%d", status);
    }
    else
    {
        app_alsa_cb.p_playback_handle = NULL;
    }
    return status;
}

/*******************************************************************************
 **
 ** Function        app_alsa_playback_write
 **
 ** Description     Write PCM sample to Playback Channel
 **
 ** Parameters      p_buffer: pointer on buffer containing
 **                 buffer_size: Buffer size
 **
 ** Returns         number of bytes written (-1 if error)
 **
 *******************************************************************************/
int app_alsa_playback_write(void *p_buffer, int buffer_size)
{
    snd_pcm_sframes_t alsa_frames;
    snd_pcm_sframes_t alsa_frames_expected;
    int bytes_per_frame;

    if (p_buffer == NULL)
    {
        return 0;
    }

    if (app_alsa_cb.p_playback_handle == NULL)
    {
        APP_ERROR0("Playback channel not opened");
        return -1;
    }

    /* Compute number of bytes per PCM samples fct(stereo, format) */
    if (app_alsa_cb.playback_param.stereo)
    {
        bytes_per_frame = 2;
    }
    else
    {
        bytes_per_frame = 1;
    }

    if ((app_alsa_cb.playback_param.format == APP_ALSA_PCM_FORMAT_S16_LE) ||
        (app_alsa_cb.playback_param.format == APP_ALSA_PCM_FORMAT_S16_BE))
    {
        bytes_per_frame *= 2;
    }
    /* Compute the number of frames */
    alsa_frames_expected = buffer_size / bytes_per_frame;

    alsa_frames = snd_pcm_writei(app_alsa_cb.p_playback_handle, p_buffer, alsa_frames_expected);
    if (alsa_frames < 0)
    {
        APP_DEBUG1("snd_pcm_recover %d", (int)alsa_frames);
        alsa_frames = snd_pcm_recover(app_alsa_cb.p_playback_handle, alsa_frames, 0);
    }
    if (alsa_frames < 0)
    {
        APP_ERROR1("snd_pcm_writei failed:%s", snd_strerror(alsa_frames));
    }
    if (alsa_frames > 0 && alsa_frames < alsa_frames_expected)
    {
        APP_DEBUG1("Short write (expected %d, wrote %d)",
                (int) alsa_frames_expected, (int)alsa_frames);
    }
    return alsa_frames * bytes_per_frame;
}

/*******************************************************************************
 **
 ** Function        app_alsa_capture_open_init
 **
 ** Description     Init ALSA Capture parameters (from microphone)
 **
 ** Parameters      p_open: Capture parameters
 **
 ** Returns         status
 **
 *******************************************************************************/
int app_alsa_capture_open_init(tAPP_ALSA_CAPTURE_OPEN *p_open)
{
    memset(p_open, 0, sizeof(*p_open));

    return 0;
}

/*******************************************************************************
 **
 ** Function        app_alsa_capture_open
 **
 ** Description     Open ALSA Capture channel (from microphone)
 **
 ** Parameters      p_open: Capture parameters
 **
 ** Returns         status
 **
 *******************************************************************************/
int app_alsa_capture_open(tAPP_ALSA_CAPTURE_OPEN *p_open)
{
    int mode = 0; /* Default is blocking */
    unsigned int nb_channels;
    int rv;
    snd_pcm_format_t format;
    snd_pcm_access_t access;

    APP_DEBUG0("Opening ALSA/Asound audio driver Capture");

    /* Sanity check if already opened */
    if (app_alsa_cb.p_capture_handle != NULL)
    {
        APP_DEBUG0("Capture was already opened");
    }

    /* check PCM Format parameter */
    format = app_alsa_get_pcm_format(p_open->format);
    if (format == SND_PCM_FORMAT_UNKNOWN)
    {
        return -1;
    }

    /* Check PCM access parameter */
    if (p_open->access == APP_ALSA_PCM_ACCESS_RW_INTERLEAVED)
    {
        access = SND_PCM_ACCESS_RW_INTERLEAVED;
    }
    else
    {
        APP_ERROR1("Unsupported PCM access:%d", p_open->access);
        return -1;
    }
    /* Check Blocking parameter */
    if (p_open->blocking == FALSE)
    {
        mode = SND_PCM_NONBLOCK;
    }

    /* check Stereo parameter */
    if (p_open->stereo)
    {
        nb_channels = 2;
    }
    else
    {
        nb_channels = 1;
    }

    /* Save the Capture open parameters */
    memcpy(&app_alsa_cb.capture_param, p_open, sizeof(app_alsa_cb.capture_param));

    /* Open ALSA driver */
    rv = snd_pcm_open(&app_alsa_cb.p_capture_handle, alsa_device,
            SND_PCM_STREAM_CAPTURE, mode);
    if (rv < 0)
    {
        APP_ERROR1("unable to open ALSA device in Capture mode:%s", snd_strerror(rv));
        return rv;
    }
    APP_DEBUG0("ALSA driver opened in Capture mode");

    /* Configure ALSA driver with PCM parameters */
    rv = snd_pcm_set_params(app_alsa_cb.p_capture_handle,
            format,
            access,
            nb_channels,
            p_open->sample_rate,
            1, /* SW resample */
            p_open->latency);
    if (rv)
    {
        APP_ERROR1("Unable to config ALSA device:%s", snd_strerror(rv));
        snd_pcm_close(app_alsa_cb.p_capture_handle);
        app_alsa_cb.p_capture_handle = NULL;
        return rv;
    }
    return 0;
}

#ifdef APP_AV_BCST_PLAY_LOOPDRV_INCLUDED
/*******************************************************************************
 **
 ** Function        app_alsa_capture_loopback_open
 **
 ** Description     Open ALSA Capture channel (from loopback driver)
 **
 ** Parameters      p_open: Capture parameters
 **
 ** Returns         status
 **
 *******************************************************************************/
int app_alsa_capture_loopback_open(tAPP_ALSA_CAPTURE_OPEN *p_open)
{
    int mode = 0; /* Default is blocking */
    unsigned int nb_channels;
    int rv;
    snd_pcm_format_t format;
    snd_pcm_access_t access;

    APP_DEBUG0("Opening ALSA/Asound audio driver Capture");

    /* Sanity check if already opened */
    if (app_alsa_cb.p_capture_handle != NULL)
    {
        APP_DEBUG0("Capture was already opened");
    }

    /* check PCM Format parameter */
    format = app_alsa_get_pcm_format(p_open->format);
    if (format == SND_PCM_FORMAT_UNKNOWN)
    {
        return -1;
    }

    /* Check PCM access parameter */
    if (p_open->access == APP_ALSA_PCM_ACCESS_RW_INTERLEAVED)
    {
        access = SND_PCM_ACCESS_RW_INTERLEAVED;
    }
    else
    {
        APP_ERROR1("Unsupported PCM access:%d", p_open->access);
        return -1;
    }
    /* Check Blocking parameter */
    if (p_open->blocking == FALSE)
    {
        mode = SND_PCM_NONBLOCK;
    }

    /* check Stereo parameter */
    if (p_open->stereo)
    {
        nb_channels = 2;
    }
    else
    {
        nb_channels = 1;
    }

    /* Save the Capture open parameters */
    memcpy(&app_alsa_cb.capture_param, p_open, sizeof(app_alsa_cb.capture_param));

    /* Open ALSA driver */
    rv = snd_pcm_open(&app_alsa_cb.p_capture_handle, alsa_device_loopback,
            SND_PCM_STREAM_CAPTURE, mode);
    if (rv < 0)
    {
        APP_ERROR1("unable to open ALSA loopback device in Capture mode:%s", snd_strerror(rv));
        return rv;
    }
    APP_DEBUG0("ALSA loopback driver opened in Capture mode");

    /* Configure ALSA driver with PCM parameters */
    rv = snd_pcm_set_params(app_alsa_cb.p_capture_handle,
            format,
            access,
            nb_channels,
            p_open->sample_rate,
            1, /* SW resample */
            p_open->latency);
    if (rv)
    {
        APP_ERROR1("Unable to config ALSA device:%s", snd_strerror(rv));
        snd_pcm_close(app_alsa_cb.p_capture_handle);
        app_alsa_cb.p_capture_handle = NULL;
        return rv;
    }
    return 0;
}
#endif /* #ifdef APP_AV_BCST_PLAY_LOOPDRV_INCLUDED */

/*******************************************************************************
 **
 ** Function        app_alsa_capture_close
 **
 ** Description     close ALSA Capture channel (from Microphone)
 **
 ** Parameters      none
 **
 ** Returns         status
 **
 *******************************************************************************/
int app_alsa_capture_close(void)
{
    int status;

    if (app_alsa_cb.p_capture_handle == NULL)
    {
        APP_ERROR0("Capture channel was not opened");
        return -1;
    }
    APP_DEBUG0("ALSA driver: Closing Capture mode");

    status = snd_pcm_close(app_alsa_cb.p_capture_handle);
    if (status < 0)
    {
        APP_ERROR1("snd_pcm_close error status:%d", status);
    }
    return status;
}

/*******************************************************************************
 **
 ** Function        app_alsa_capture_read
 **
 ** Description     Read PCM samples from Capture Channel
 **
 ** Parameters      p_buffer: pointer on buffer containing
 **                 buffer_size: size of the buffer
 **
 ** Returns         number of bytes read (< 0 if error)
 **
 *******************************************************************************/
int app_alsa_capture_read(void *p_buffer, int buffer_size)
{
    snd_pcm_sframes_t alsa_frames;
    snd_pcm_sframes_t alsa_frames_expected;
    int bytes_per_frame;

    if (p_buffer == NULL)
    {
        return 0;
    }

    if (app_alsa_cb.p_capture_handle == NULL)
    {
        APP_ERROR0("Capture channel not opened");
        return -1;
    }

    /* Compute number of bytes per PCM samples fct(stereo, format) */
    if (app_alsa_cb.capture_param.stereo)
    {
        bytes_per_frame = 2;
    }
    else
    {
        bytes_per_frame = 1;
    }

    if ((app_alsa_cb.capture_param.format == APP_ALSA_PCM_FORMAT_S16_LE) ||
        (app_alsa_cb.capture_param.format == APP_ALSA_PCM_FORMAT_S16_BE))
    {
        bytes_per_frame *= 2;
    }

    /* Divide by the number of channel and by */
    alsa_frames_expected = buffer_size / bytes_per_frame;

    alsa_frames = snd_pcm_readi(app_alsa_cb.p_capture_handle, p_buffer, alsa_frames_expected);
    if (alsa_frames < 0)
    {
        if (alsa_frames == -EAGAIN)
        {
            APP_ERROR1("snd_pcm_readi returns:%d (EAGAIN)", (int)alsa_frames);
            /* This is not really an error */
            alsa_frames = 0;
        }
        else
        {
            APP_ERROR1("snd_pcm_readi returns:%d", (int)alsa_frames);
        }
    }
    else if ((alsa_frames >= 0) &&
             (alsa_frames < alsa_frames_expected))
    {
        APP_DEBUG1("Short read (expected %i, read %i)",
            (int) alsa_frames_expected, (int)alsa_frames);
    }

    return alsa_frames * bytes_per_frame;
}

/*******************************************************************************
 **
 ** Function        app_alsa_get_pcm_format
 **
 ** Description     Check and Convert the PCM format asked by the application to
 **                 ALSA PCM format
 **
 ** Parameters      app_format: PCM format asked by the application
 **
 ** Returns         Alsa PCM format
 **
 *******************************************************************************/
static snd_pcm_format_t app_alsa_get_pcm_format(tAPP_ALSA_PCM_FORMAT app_format)
{
    snd_pcm_format_t format;

    switch (app_format)
    {
    case APP_ALSA_PCM_FORMAT_S16_LE:
        format = SND_PCM_FORMAT_S16_LE;
        break;
    case APP_ALSA_PCM_FORMAT_S16_BE:
        format = SND_PCM_FORMAT_S16_BE;
        break;
    case APP_ALSA_PCM_FORMAT_U8:
        format = SND_PCM_FORMAT_U8;
        break;
    default:
        format = SND_PCM_FORMAT_UNKNOWN;
        APP_ERROR1("Unsupported PCM format:%d", app_format);
        break;
    }
    return format;
}

