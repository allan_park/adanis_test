/*****************************************************************************
**
**  Name:           app_hs.c
**
**  Description:    Bluetooth Manager application
**
**  Copyright (c) 2009-2012, Broadcom Corp., All Rights Reserved.
**  Broadcom Bluetooth Core. Proprietary and confidential.
**
*****************************************************************************/
#include "buildcfg.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "bsa_api.h"

#include "gki.h"
#include "uipc.h"

#include "app_utils.h"
#include "app_xml_param.h"
#include "app_xml_utils.h"

#include "app_disc.h"

#include "app_hs.h"
#include "app_dm.h"
#include "app_wav.h"
#include "btm_api.h"
#include "bta_api.h"

#ifdef PCM_ALSA
    #include "alsa/asoundlib.h"

#ifdef ADANIS_ALSA_WRAPPER
    #include "app_alsa.h"
#endif
#ifdef ADANIS_ALSA_LATENCY
    #include "alsa_latency.h"
#endif
#endif /* PCM_ALSA */

#ifndef BSA_SCO_ROUTE_DEFAULT
#define BSA_SCO_ROUTE_DEFAULT BSA_SCO_ROUTE_HCI
#endif

/* ui keypress definition */
enum
{
    APP_HS_KEY_OPEN = 1,
    APP_HS_KEY_CLOSE,
    APP_HS_KEY_PRESS,
    APP_HS_KEY_PLAY,
    APP_HS_KEY_RECORD,
    APP_HS_KEY_STOP_RECORDING,
    APP_HS_KEY_QUIT = 99
};

#define APP_HS_SAMPLE_RATE      8000      /* HS Voice sample rate is always 8KHz */
#define APP_HS_BITS_PER_SAMPLE  16        /* HS Voice sample size is 16 bits */
#define APP_HS_CHANNEL_NB       1         /* HS Voice sample in mono */

#define APP_HS_FEATURES  ( BSA_HS_FEAT_ECNR | BSA_HS_FEAT_3WAY | BSA_HS_FEAT_CLIP | \
                           BSA_HS_FEAT_VREC | BSA_HS_FEAT_RVOL | BSA_HS_FEAT_ECS | \
                           BSA_HS_FEAT_ECC | BSA_HS_FEAT_CODEC | BSA_HS_FEAT_UNAT )

#define APP_HS_MIC_VOL  7
#define APP_HS_SPK_VOL  7


#define APP_HS_HSP_SERVICE_NAME "BSA Headset"
#define APP_HS_HFP_SERVICE_NAME "BSA Handsfree"

#define APP_HS_MAX_AUDIO_BUF 240

#define APP_HS_XML_REM_DEVICES_FILE_PATH       "./bt_devices.xml"

/*
 * Types
 */

/* control block (not needed to be stored in NVRAM) */
typedef struct
{
    tBSA_HS_CONN_CB    conn_cb[BSA_HS_MAX_NUM_CONN];
    BOOLEAN            registered;
    BOOLEAN            is_muted;
    BOOLEAN            mute_inband_ring;
    UINT32             ring_handle;
    UINT32             cw_handle;
    UINT32             call_op_handle;
    UINT8              sco_route;
    int                rec_fd; /* recording file descriptor */
    int                data_size;
    short              audio_buf[APP_HS_MAX_AUDIO_BUF];
#ifdef PCM_ALSA
#ifdef ADANIS_ALSA_WRAPPER
    int                audio_dummy;
    char               *audio_i2s_buffer_b;
    char               *audio_i2s_buffer;
#endif
#if defined( ADANIS_ALSA_LATENCY ) || defined( ADANIS_ALSA_WRAPPER )
    BOOLEAN            audio_I2S_SCO_looping;
#endif
#ifdef ADANIS_ALSA_LATENCY
    tEX_ALSA_GOSTOP    alsa_cp_stop_flag;
#endif
#endif /* PCM_ALSA */
    BOOLEAN            open_pending;
    BD_ADDR            open_pending_bda;
} tAPP_HS_CB;

/*
 * Globales Variables
 */

tAPP_HS_CB  app_hs_cb = {0x00,};

const char *app_hs_service_ind_name[] =
{
    "NO SERVICE",
    "SERVICE AVAILABLE"
};

const char *app_hs_call_ind_name[] =
{
    "NO CALL",
    "ACTIVE CALL"
};

const char *app_hs_callsetup_ind_name[] =
{
    "CALLSETUP DONE",
    "INCOMING CALL",
    "OUTGOING CALL",
    "ALERTING REMOTE"
};

const char *app_hs_callheld_ind_name[] =
{
    "NONE ON-HOLD",
    "ACTIVE+HOLD",
    "ALL ON-HOLD"
};

const char *app_hs_roam_ind_name[] =
{
    "HOME NETWORK",
    "ROAMING"
};

/* application callback */
static tHsCallback *s_pHsCallback = NULL;

#ifdef PCM_ALSA
static char      *alsa_device = "default"; /* ALSA playback device */
static snd_pcm_t *alsa_handle_playback = NULL;
static snd_pcm_t *alsa_handle_capture  = NULL;
static BOOLEAN    alsa_capture_opened  = FALSE;
static BOOLEAN    alsa_playback_opened = FALSE;
#endif /* PCM_ALSA */

#ifdef PCM_ALSA
int app_hs_open_alsa_duplex(void);
int app_hs_close_alsa_duplex(void);
#endif /* PCM_ALSA */

/* ADANIS ADD */
static int       playback_rate          = 8000; /* 16000; */

#ifdef PCM_ALSA
static pthread_t p_thread_alsa_sco      = NULL;
#define I2S_RESAMPLE (0x01)
#define I2S_FORMAT   (SND_PCM_FORMAT_S16_LE)
#define I2S_CHANNELS (0x02) //(0x02)
#define I2S_PERIOD_SIZE (0x00)
#define I2S_BLOCK       (0x00)
#define I2S_BUFFER_SIZE (0x00)
#define I2S_LATENCY_MIN (2000) // 32;  /* in frames / 2 */
#define I2S_LATENCY_MAX (2048)         /* in frames / 2 */

// for I2S loopback in testing
//#define LOOPBACK_SCO_I2S

// for extended snd pcm parameter settings
//#define USE_I2S_SETUP_EXT_PARAMS

typedef enum {
  EX_LATENCY_RTN_SUCCESS = 0x00,
  EX_LATENCY_RTN_ERROR   = -1,
  EX_LATENCY_RTN_EXIT    = -2,
} tEX_LATENCY;

#ifdef ADANIS_ALSA_WRAPPER
#ifdef USE_I2S_SETUP_EXT_PARAMS
static int adanis_i2s_setparams_stream(snd_pcm_t *handle,
                     snd_pcm_hw_params_t *params,
                     const char *id) {
    int err;
    unsigned int rrate;
    err = snd_pcm_hw_params_any(handle, params);
    if (err < 0) {
        printf("Broken configuration for %s PCM: no configurations available: %s\n", snd_strerror(err), id);
        return err;
    }
    err = snd_pcm_hw_params_set_rate_resample(handle, params, I2S_RESAMPLE);
    if (err < 0) {
        printf("Resample setup failed for %s (val %i): %s\n", id, I2S_RESAMPLE, snd_strerror(err));
        return err;
    }
    err = snd_pcm_hw_params_set_access(handle, params, SND_PCM_ACCESS_RW_INTERLEAVED);
    if (err < 0) {
        printf("Access type not available for %s: %s\n", id, snd_strerror(err));
        return err;
    }
    err = snd_pcm_hw_params_set_format(handle, params, I2S_FORMAT);
    if (err < 0) {
        printf("Sample format not available for %s: %s\n", id, snd_strerror(err));
        return err;
    }
    
    err = snd_pcm_hw_params_set_channels(handle, params, I2S_CHANNELS);
    if (err < 0) {
        printf("Channels count (%i) not available for %s: %s\n", I2S_CHANNELS, id, snd_strerror(err));
        return err;
    }
    rrate = playback_rate;
    err = snd_pcm_hw_params_set_rate_near(handle, params, &rrate, 0);
    if (err < 0) {
        printf("Rate %iHz not available for %s: %s\n", playback_rate, id, snd_strerror(err));
        return err;
    }
    if ((int)rrate != playback_rate) {
        printf("Rate doesn't match (requested %iHz, get %iHz)\n", playback_rate, err);
        return -EINVAL;
    }
    return 0;
}

static int adanis_i2s_setparams_bufsize(snd_pcm_t *handle,
                      snd_pcm_hw_params_t *params,
                      snd_pcm_hw_params_t *tparams,
                      snd_pcm_uframes_t bufsize,
                      const char *id) {
    int err;
    snd_pcm_uframes_t periodsize;
    snd_pcm_hw_params_copy(params, tparams);
    periodsize = bufsize * 2;
    err = snd_pcm_hw_params_set_buffer_size_near(handle, params, &periodsize);
    if (err < 0) {
        printf("Unable to set buffer size %li for %s: %s\n", bufsize * 2, id, snd_strerror(err));
        return err;
    }
    if (I2S_PERIOD_SIZE > 0)
        periodsize = I2S_PERIOD_SIZE;
    else
        periodsize /= 2;
    err = snd_pcm_hw_params_set_period_size_near(handle, params, &periodsize, 0);
    if (err < 0) {
        printf("Unable to set period size %li for %s: %s\n", periodsize, id, snd_strerror(err));
        return err;
    }
    return 0;
}

static int adanis_i2s_setparams_set(snd_pcm_t *handle,
                  snd_pcm_hw_params_t *params,
                  snd_pcm_sw_params_t *swparams,
                  const char *id)
{
    int err;
    snd_pcm_uframes_t val;
    err = snd_pcm_hw_params(handle, params);
    if (err < 0) {
        printf("Unable to set hw params for %s: %s\n", id, snd_strerror(err));
        return err;
    }
    err = snd_pcm_sw_params_current(handle, swparams);
    if (err < 0) {
        printf("Unable to determine current swparams for %s: %s\n", id, snd_strerror(err));
        return err;
    }
    err = snd_pcm_sw_params_set_start_threshold(handle, swparams, 0x7fffffff);
    if (err < 0) {
        printf("Unable to set start threshold mode for %s: %s\n", id, snd_strerror(err));
        return err;
    }
    if (!I2S_BLOCK) {
        val = 4;
    } else {
        snd_pcm_hw_params_get_period_size(params, &val, NULL);
        printf("[info] setparams_set :: period_size[%d]\n", (int)val);
    }
    err = snd_pcm_sw_params_set_avail_min(handle, swparams, val);
    if (err < 0) {
        printf("Unable to set avail min for %s: %s\n", id, snd_strerror(err));
        return err;
    }
    err = snd_pcm_sw_params(handle, swparams);
    if (err < 0) {
        printf("Unable to set sw params for %s: %s\n", id, snd_strerror(err));
        return err;
    }
    return 0;
}

static int adanis_i2s_setparams(snd_pcm_t *phandle, snd_pcm_t *chandle, int *bufsize) {
    int err, last_bufsize = *bufsize;
    snd_pcm_hw_params_t *pt_params, *ct_params;     /* templates with rate, format and channels */
    snd_pcm_hw_params_t *p_params, *c_params;
    snd_pcm_sw_params_t *p_swparams, *c_swparams;
    snd_pcm_uframes_t p_size, c_size, p_psize, c_psize;
    unsigned int p_time, c_time;
    unsigned int val;
    snd_pcm_hw_params_alloca(&p_params);
    snd_pcm_hw_params_alloca(&c_params);
    snd_pcm_hw_params_alloca(&pt_params);
    snd_pcm_hw_params_alloca(&ct_params);
    snd_pcm_sw_params_alloca(&p_swparams);
    snd_pcm_sw_params_alloca(&c_swparams);
    if ((err = adanis_i2s_setparams_stream(phandle, pt_params, "playback")) < 0) {
        printf("Unable to set parameters for playback stream: %s\n", snd_strerror(err));
        return EX_LATENCY_RTN_EXIT;
    }
    if ((err = adanis_i2s_setparams_stream(chandle, ct_params, "capture")) < 0) {
        printf("Unable to set parameters for capture stream: %s\n", snd_strerror(err));
        return EX_LATENCY_RTN_EXIT;
    }
    if (I2S_BUFFER_SIZE > 0) {
        *bufsize = I2S_BUFFER_SIZE;
        goto __set_it;
    }
  __again:
    if (I2S_BUFFER_SIZE > 0) {
        printf("[error] buffer_size:%d\n", I2S_BUFFER_SIZE);
        return EX_LATENCY_RTN_ERROR/*-1*/;
    }
    if (last_bufsize == *bufsize)
        *bufsize += 4;
    last_bufsize = *bufsize;
    if (*bufsize > I2S_LATENCY_MAX) {
        printf("[error]parameters last_bufsize:%d, latency_max:%d\n", last_bufsize, I2S_LATENCY_MAX);
        return EX_LATENCY_RTN_ERROR/*-1*/;
    }
  __set_it:
    if ((err = adanis_i2s_setparams_bufsize(phandle, p_params, pt_params, *bufsize, "playback")) < 0) {
        printf("Unable to set sw parameters for playback stream: %s\n", snd_strerror(err));
        return EX_LATENCY_RTN_EXIT;
    }
    if ((err = adanis_i2s_setparams_bufsize(chandle, c_params, ct_params, *bufsize, "capture")) < 0) {
        printf("Unable to set sw parameters for playback stream: %s\n", snd_strerror(err));
        return EX_LATENCY_RTN_EXIT;
    }

    snd_pcm_hw_params_get_period_size(p_params, &p_psize, NULL);
    snd_pcm_hw_params_get_period_size(c_params, &c_psize, NULL);
#if 1 /* __PERIOD_TIME_PERIOD_SIZE_BUFFER_SIZE__ */
    printf("[info] period_size :: p_params[%d], c_params[%d]\n", p_psize, c_psize);
    p_psize = 32;
    c_psize = 32;
#endif
    if (p_psize > (unsigned int)*bufsize) {
        *bufsize = p_psize;
    }
    
    // snd_pcm_hw_params_get_period_size(c_params, &c_psize, NULL);
    if (c_psize > (unsigned int)*bufsize) {
        *bufsize = c_psize;
    }
    
    snd_pcm_hw_params_get_period_time(p_params, &p_time, NULL);
    snd_pcm_hw_params_get_period_time(c_params, &c_time, NULL);
#if 1 /* __PERIOD_TIME_PERIOD_SIZE_BUFFER_SIZE__ */
    printf("[info] period_time :: p_params[%d], c_params[%d]\n", p_time, c_time);
    p_time = 725;
    c_time = 725;
#endif
    if (p_time != c_time) {
        goto __again;
    }
    
    snd_pcm_hw_params_get_buffer_size(p_params, &p_size);
    snd_pcm_hw_params_get_buffer_size(c_params, &c_size);
#if 1 /* __PERIOD_TIME_PERIOD_SIZE_BUFFER_SIZE__ */
    printf("[info] buffer_size :: p_params[%d], c_params[%d]\n", p_size, c_size);
    p_size = 64;
    c_size = 64;
#endif
    if (p_psize * 2 < p_size) {
        snd_pcm_hw_params_get_periods_min(p_params, &val, NULL);
        if (val > 2) {
            printf("playback device does not support 2 periods per buffer\n");
            return EX_LATENCY_RTN_EXIT;
        }
        goto __again;
    }
    // snd_pcm_hw_params_get_buffer_size(c_params, &c_size);
    if (c_psize * 2 < c_size) {
        snd_pcm_hw_params_get_periods_min(c_params, &val, NULL);
        if (val > 2 ) {
            printf("capture device does not support 2 periods per buffer\n");
            return EX_LATENCY_RTN_EXIT;
        }
        goto __again;
    }
    if ((err = adanis_i2s_setparams_set(phandle, p_params, p_swparams, "playback")) < 0) {
        printf("Unable to set sw parameters for playback stream: %s\n", snd_strerror(err));
        return EX_LATENCY_RTN_EXIT;
    }
    if ((err = adanis_i2s_setparams_set(chandle, c_params, c_swparams, "capture")) < 0) {
        printf("Unable to set sw parameters for capture stream: %s\n", snd_strerror(err));
        return EX_LATENCY_RTN_EXIT;
    }
    if ((err = snd_pcm_prepare(phandle)) < 0) {
        printf("Prepare error: %s\n", snd_strerror(err));
        return EX_LATENCY_RTN_EXIT;
    }

#if 0
    if(dump_hw_params) {
        snd_pcm_dump(phandle, output);
        snd_pcm_dump(chandle, output);
    }
    fflush(stdout);
#endif

    return EX_LATENCY_RTN_SUCCESS/*0*/;
}
#endif /* endif USE_I2S_SETUP_EXT_PARAMS */
#endif /* endif ADANIS_ALSA_WRAPPER */

/*******************************************************************************
 **
 ** Function         alsa_sleep_ms
 **
 ** Description      just milliseconds sleep function
 **
 ** Parameter        
 **
 ** Returns          
 *******************************************************************************/
static void adanis_sleep_ms(int milliseconds) { // cross-platform sleep function
#ifdef WIN32
    Sleep(milliseconds);
#elif _POSIX_C_SOURCE >= 199309L
    struct timespec ts;
    ts.tv_sec = milliseconds / 1000;
    ts.tv_nsec = (milliseconds % 1000) * 1000000;
    nanosleep(&ts, NULL);
#else
    usleep(milliseconds * 1000);
#endif
}

#ifdef ADANIS_ALSA_WRAPPER
static long adanis_alsa_readbuf(snd_pcm_t *handle, char *buf, long len, size_t *frames, size_t *max) {
    long r = 0x00;
    /* NON_BLOCKING */
    do {
        r = snd_pcm_readi(handle, buf, len);
    } while (r == -EAGAIN);
    if (r > 0) {
        *frames += r;
        if ((long)*max < r)
            *max = r;
    }
    // printf("read = %li\n", r);
    return r;
}
#endif /* endif ADANIS_ALSA_WRAPPER */

#if defined( ADANIS_ALSA_LATENCY ) || defined( ADANIS_ALSA_WRAPPER )
/*******************************************************************************
 **
 ** Function         adanis_i2s_thread_alsa_SCO
 **
 ** Description      run loop for SCO data pumping
 **                     BT I2S RX -> TX[SPK],
 **                     RX[MIC]   -> BT I2S TX
 **
 ** Parameter        
 **
 ** Returns          
 *******************************************************************************/
static void *adanis_i2s_thread_alsa_SCO(void *data) {
    tAPP_HS_CB  *app_cb = (tAPP_HS_CB  *)data;
#ifdef ADANIS_ALSA_WRAPPER
    char *buffer_b      = NULL;
    char *buffer        = NULL;
    tADANIS_ALSA_CB* ALSA_CB = NULL;

    snd_pcm_t *phandle = NULL;
    snd_pcm_t *chandle = NULL;
    snd_pcm_t *phandle_b = NULL;
    snd_pcm_t *chandle_b = NULL;
    
    ssize_t r;
    size_t frames_in, frames_out, in_max;
    int latency;
#endif
    APP_DEBUG0("[hs] adanis_i2s_thread_alsa_SCO IN !!!");

    while(TRUE) {
        if(app_cb->audio_I2S_SCO_looping) {
            int ok;
            APP_DEBUG1("[hs] adanis_i2s_thread_alsa_SCO playback_rate:%d !!!", playback_rate);
#ifdef ADANIS_ALSA_WRAPPER
            ALSA_CB = adanis_alsa_get_cb_handle();

            latency   = I2S_LATENCY_MIN;
            frames_in = frames_out = 0x00;

#ifdef LOOPBACK_SCO_I2S
            phandle   = ALSA_CB->p_playback_handle_I2S;
            chandle   = ALSA_CB->p_capture_handle_I2S;
            
            phandle_b = ALSA_CB->p_playback_handle;
            chandle_b = ALSA_CB->p_capture_handle;
#else
            phandle   = ALSA_CB->p_playback_handle_I2S;
            chandle   = ALSA_CB->p_capture_handle;

            phandle_b = ALSA_CB->p_playback_handle;
            chandle_b = ALSA_CB->p_capture_handle_I2S;
#endif
            buffer_b  = app_cb->audio_i2s_buffer_b;
            buffer    = app_cb->audio_i2s_buffer;

            APP_DEBUG0("[hs] adanis thread looping [IN] !!!");
            while(app_cb->audio_I2S_SCO_looping == TRUE) {
                ok = 0x01;
                if ((r = adanis_alsa_readbuf(chandle, buffer, latency, &frames_in, &in_max)) < 0x00){
                    ok = 0x00;
                    //APP_DEBUG0("[hs] r_a faile 1");
                } else {
                    //APP_DEBUG1("[hs] r_a[%d]", r);
                    if (adanis_alsa_writebuf(phandle, buffer, r, &frames_out) < 0x00) {
                        ok = 0x00;
                        //APP_DEBUG0("[hs] r_a faile 2");
                    }
                }

                ok = 0x01;
                if ((r = adanis_alsa_readbuf(chandle_b, buffer_b, latency, &frames_in, &in_max)) < 0x00) {
                    ok = 0x00;
                    //APP_DEBUG0("[hs] r_a faile 3");
                } else {
                    //APP_DEBUG1("[hs] r_b [%d]", r);
                    if (adanis_alsa_writebuf(phandle_b, buffer_b, r, &frames_out) < 0x00) {
                        ok = 0x00;
                        //APP_DEBUG0("[hs] r_a faile 4");
                    }
                }
            }

            APP_DEBUG0("[hs] adanis thread looping [OUT] !!!");
#else  /* else ADANIS_ALSA_WRAPPER */
            app_cb->alsa_cp_stop_flag = EX_ALSA_GO;
            alsa_link_capture_playback(&(app_cb->alsa_cp_stop_flag), playback_rate);
#endif /* endif ADANIS_ALSA_WRAPPER */
        } else {
            // APP_DEBUG0("[hs] adanis_thread_alsa_I2S_SCO sleep !!!");
            adanis_sleep_ms(100);
        }
    }

    p_thread_alsa_sco = NULL;
    pthread_exit(NULL);
    APP_DEBUG0("[hs] adanis_i2s_thread_alsa_SCO OUT !!!");
}

/*******************************************************************************
 **
 ** Function         adanis_i2s_init_thread_alsa_SCO
 **
 ** Description      create alsa thread and malloc alsa buffer memory
 **
 ** Parameter        
 **
 ** Returns          TRUE/FALSE
 *******************************************************************************/
static int adanis_i2s_init_thread_alsa_SCO(void) {
    int thr_id;
    int buffer_size;
#ifdef ADANIS_ALSA_WRAPPER
    char *buffer_b = NULL;
    char *buffer   = NULL;

    buffer_size = (I2S_LATENCY_MAX * snd_pcm_format_width(I2S_FORMAT) / 0x08) * I2S_CHANNELS;

    buffer     = malloc(buffer_size);
    if(buffer == NULL) {
        APPL_TRACE_EVENT0("[hs] no memory !!\n");
        return FALSE;
    }
    
    buffer_b   = malloc(buffer_size);
    if(buffer_b == NULL) {
        free(buffer);
        buffer = NULL;
        APPL_TRACE_EVENT0("[hs] no memory[B] !!\n");
        return FALSE;
    }
#else
    buffer_size = 0x00;
#endif

    APPL_TRACE_EVENT1("[hs] adanis_i2s_init_thread_alsa_SCO pthread_create %d !!", buffer_size);
    thr_id = pthread_create(&p_thread_alsa_sco, NULL, &adanis_i2s_thread_alsa_SCO, (void *)&app_hs_cb);
    if (thr_id < 0x00) {
        APPL_TRACE_EVENT0("[hs] adanis_i2s_init_thread_alsa_SCO ERROR !!");
        return FALSE;
    }

#ifdef ADANIS_ALSA_WRAPPER
    memset(buffer,   0x00, buffer_size);
    memset(buffer_b, 0x00, buffer_size);
    
    app_hs_cb.audio_i2s_buffer   = buffer;
    app_hs_cb.audio_i2s_buffer_b = buffer_b;
#endif
    APPL_TRACE_EVENT0("[hs] adanis_i2s_init_thread_alsa_SCO OK !!");
    return TRUE;
}
#endif

#ifdef ADANIS_ALSA_WRAPPER

#ifdef USE_I2S_SETUP_EXT_PARAMS
/*******************************************************************************
 **
 ** Function         adanis_i2s_free_pcm_handle
 **
 ** Description      de-init alsa resource in case of using alsa extended parameter settings
 **
 ** Parameter        
 **
 ** Returns          void
 *******************************************************************************/
static void adanis_i2s_free_pcm_handle(void) {
    tAPP_HS_CB  *app_cb = &app_hs_cb;
    tADANIS_ALSA_CB* ALSA_CB = NULL;
    snd_pcm_t *phandle = NULL;
    snd_pcm_t *chandle = NULL;
    snd_pcm_t *phandle_b = NULL;
    snd_pcm_t *chandle_b = NULL;

    ALSA_CB = adanis_alsa_get_cb_handle();
    if(ALSA_CB == NULL) {
        return;
    }

    phandle   = ALSA_CB->p_playback_handle_I2S;
    chandle   = ALSA_CB->p_capture_handle;

    phandle_b = ALSA_CB->p_playback_handle;
    chandle_b = ALSA_CB->p_capture_handle_I2S;
    

    if(phandle) {
        snd_pcm_hw_free(phandle);
    }

    if(chandle) {
        snd_pcm_unlink(chandle);
        snd_pcm_hw_free(chandle);
    }

    if(phandle_b) {
        snd_pcm_hw_free(phandle_b);
    }

    if(chandle_b) {
        snd_pcm_unlink(chandle_b);
        snd_pcm_hw_free(chandle_b);
    }
}

#define SND_PCM_FORMAT_SET_SILENCE
/*******************************************************************************
 **
 ** Function         adanis_i2s_setup_thread_alsa_SCO
 **
 ** Description      setup alsa resource in case of using alsa extended parameter settings
 **
 ** Parameter        
 **
 ** Returns          void
 *******************************************************************************/
static int adanis_i2s_setup_thread_alsa_SCO(void) {
    int ok, rtn_err;
    int err, latency, morehelp;
    tADANIS_ALSA_CB* ALSA_CB = NULL;
#ifdef SND_PCM_FORMAT_SET_SILENCE
    tAPP_HS_CB  *app_cb = &app_hs_cb;

    char *buffer_b      = NULL;
    char *buffer        = NULL;
    size_t frames_in, frames_out, in_max;
#endif

    snd_pcm_t *phandle = NULL;
    snd_pcm_t *chandle = NULL;
    snd_pcm_t *phandle_b = NULL;
    snd_pcm_t *chandle_b = NULL;
    
    APP_DEBUG0("adanis_i2s_setup_thread_alsa_SCO !!");
    
    ALSA_CB = adanis_alsa_get_cb_handle();
    if(ALSA_CB == NULL) {
        return -1;
    }

    phandle   = ALSA_CB->p_playback_handle_I2S;
    chandle   = ALSA_CB->p_capture_handle;

    phandle_b = ALSA_CB->p_playback_handle;
    chandle_b = ALSA_CB->p_capture_handle_I2S;
    
#ifdef SND_PCM_FORMAT_SET_SILENCE
    buffer_b  = app_cb->audio_i2s_buffer_b;
    buffer    = app_cb->audio_i2s_buffer;
#endif

    // init
#ifdef SND_PCM_FORMAT_SET_SILENCE
    frames_in = frames_out = 0x00;
#endif
    latency = I2S_LATENCY_MIN;
    if ((rtn_err = adanis_i2s_setparams(phandle, chandle, &latency)) < 0) {
        printf("[error] adanis_i2s_setparams %d\n", rtn_err);

        if(rtn_err == EX_LATENCY_RTN_EXIT) {
            return -1;
        }
    }

    latency = I2S_LATENCY_MIN;
    if ((rtn_err = adanis_i2s_setparams(phandle_b, chandle_b, &latency)) < 0) {
        printf("[error] adanis_i2s_setparams[B] %d\n", rtn_err);

        if(rtn_err == EX_LATENCY_RTN_EXIT) {
            return -1;
        }
    }

    if ((err = snd_pcm_link(chandle, phandle)) < 0) {
        printf("Streams link error: %s\n", snd_strerror(err));
        return -1;
    }

    if ((err = snd_pcm_link(chandle_b, phandle_b)) < 0) {
        return -1;
    }

#ifdef SND_PCM_FORMAT_SET_SILENCE
    if (snd_pcm_format_set_silence(I2S_FORMAT, buffer, latency*I2S_CHANNELS) < 0) {
        fprintf(stderr, "silence error\n");
        // break;
    }
    if (adanis_alsa_writebuf(phandle, buffer, latency, &frames_out) < 0) {
        fprintf(stderr, "write error[01]\n");
        // break;
    }
    if (adanis_alsa_writebuf(phandle, buffer, latency, &frames_out) < 0) {
        fprintf(stderr, "write error[02]\n");
        // break;
    }
#endif
    if ((err = snd_pcm_start(chandle)) < 0) {
        printf("Go error: %s\n", snd_strerror(err));
        return -1;
    }

#ifdef SND_PCM_FORMAT_SET_SILENCE
    if (adanis_alsa_writebuf(phandle_b, buffer_b, latency, &frames_out) < 0) {
        fprintf(stderr, "write error[B][01]\n");
        // break;
    }
    if (adanis_alsa_writebuf(phandle_b, buffer_b, latency, &frames_out) < 0) {
        fprintf(stderr, "write error[B][02]\n");
        // break;
    }
#endif
    
    if ((err = snd_pcm_start(chandle_b)) < 0) {
        printf("Go error[B]: %s\n", snd_strerror(err));
        return -1;
    }

    return 0x00;
}
#endif /* endif USE_I2S_SETUP_EXT_PARAMS */

#endif /* endif ADANIS_ALSA_WRAPPER */
#endif /* PCM_ALSA */

/*
* Local Function
*/

/*******************************************************************************
**
** Function         app_hs_get_default_conn
**
** Description      Find the first active connection control block
**
** Returns          Pointer to the found connection, NULL if not found
*******************************************************************************/
tBSA_HS_CONN_CB *app_hs_get_default_conn()
{
    UINT16 cb_index;

    APPL_TRACE_EVENT0("app_hs_get_default_conn");

    for(cb_index = 0; cb_index < BSA_HS_MAX_NUM_CONN; cb_index++)
    {
        if(app_hs_cb.conn_cb[cb_index].connection_active)
            return &app_hs_cb.conn_cb[cb_index];
    }
    return NULL;
}

#ifdef ADANIS_ALSA_WRAPPER
/*******************************************************************************
**
** Function         adanis_hs_get_call_status
**
** Description      get active connection control block's call status
**
** Returns          UINT16 (see BSA_HS_ST_IN_CALL)
*******************************************************************************/
UINT16 adanis_hs_get_call_status(void) {
    UINT16 status = 0x00; /* BSA_HS_ST_IN_CALL */
    tBSA_HS_CONN_CB *p_conn;

    p_conn = app_hs_get_default_conn();
    if (p_conn == NULL) {
        APP_DEBUG0("adanis_hs_get_call_status -> no connection available");
        return status;
    }

    APPL_TRACE_EVENT1("adanis_hs_get_call_status(0x%X)", p_conn->status);
    return p_conn->status;
}
#endif

/*******************************************************************************
**
** Function         app_hs_get_conn_by_handle
**
** Description      Find a connection control block by its handle
**
** Returns          Pointer to the found connection, NULL if not found
*******************************************************************************/
static tBSA_HS_CONN_CB *app_hs_get_conn_by_handle(UINT16 handle)
{
    APPL_TRACE_EVENT1("app_hs_get_conn_by_handle: %d", handle);

    /* check that the handle does not go beyond limits */
    if (handle <= BSA_HS_MAX_NUM_CONN)
    {
        return &app_hs_cb.conn_cb[handle-1];
    }

    return NULL;
}

/*******************************************************************************
**
** Function         app_hs_find_indicator_id
**
** Description      parses the indicator string and finds the position of a field
**
** Returns          index in the string
*******************************************************************************/
static UINT8 app_hs_find_indicator_id(char * ind, char * field)
{
    UINT16 string_len = strlen(ind);
    UINT8 i, id = 0;
    BOOLEAN skip = FALSE;

    for(i=0; i< string_len ; i++)
    {
        if(ind[i] == '"')
        {
            if(!skip)
            {
                id++;
                if(!strncmp(&ind[i+1], field, strlen(field)) && (ind[i+1+strlen(field)] == '"'))
                {

                    return id;
                }
                else
                {
                    /* skip the next " */
                    skip = TRUE;
                }
            }
            else
            {
                skip = FALSE;
            }
        }
    }
    return 0;
}

/*******************************************************************************
**
** Function         app_hs_decode_indicator_string
**
** Description      process the indicator string and sets the indicator ids
**
** Returns          void
*******************************************************************************/
static void app_hs_decode_indicator_string(tBSA_HS_CONN_CB *p_conn, char * ind)
{
    p_conn->call_ind_id = app_hs_find_indicator_id(ind, "call");
    p_conn->call_setup_ind_id = app_hs_find_indicator_id(ind, "callsetup");
    if(!p_conn->call_setup_ind_id)
    {
        p_conn->call_setup_ind_id = app_hs_find_indicator_id(ind, "call_setup");
    }
    p_conn->service_ind_id = app_hs_find_indicator_id(ind, "service");
    p_conn->battery_ind_id = app_hs_find_indicator_id(ind, "battchg");
    p_conn->callheld_ind_id = app_hs_find_indicator_id(ind, "callheld");
    p_conn->signal_strength_ind_id = app_hs_find_indicator_id(ind, "signal");
    p_conn->roam_ind_id = app_hs_find_indicator_id(ind, "roam");
}

/*******************************************************************************
**
** Function         app_hs_set_indicator_status
**
** Description      sets the current indicator
**
** Returns          void
*******************************************************************************/
static void app_hs_set_initial_indicator_status(tBSA_HS_CONN_CB *p_conn, char * ind)
{
    UINT8 i, pos;

    /* Clear all indicators. Not all indicators will be initialized */
    p_conn->curr_call_ind = 0;
    p_conn->curr_call_setup_ind = 0;
    p_conn->curr_service_ind = 0;
    p_conn->curr_callheld_ind = 0;
    p_conn->curr_signal_strength_ind = 0;
    p_conn->curr_roam_ind = 0;
    p_conn->curr_battery_ind = 0;

    /* skip any spaces in the front */
    while ( *ind == ' ' ) ind++;

    /* get "call" indicator*/
    pos = p_conn->call_ind_id -1;
    for(i=0; i< strlen(ind) ; i++)
    {
        if(!pos)
        {
            p_conn->curr_call_ind = ind[i] - '0';
            break;
        }
        else if(ind[i] == ',')
            pos--;
    }

    /* get "callsetup" indicator*/
    pos = p_conn->call_setup_ind_id -1;
    for(i=0; i< strlen(ind) ; i++)
    {
        if(!pos)
        {
            p_conn->curr_call_setup_ind = ind[i] - '0';
            break;
        }
        else if(ind[i] == ',')
            pos--;
    }

    /* get "service" indicator*/
    pos = p_conn->service_ind_id -1;
    for(i=0; i< strlen(ind) ; i++)
    {
        if(!pos)
        {
            p_conn->curr_service_ind = ind[i] - '0';
            /* if there is no service play the designated tone */
            if(!p_conn->curr_service_ind)
            {
                /*
                if(HS_CFG_BEEP_NO_NETWORK)
                {
                    UTL_BeepPlay(HS_CFG_BEEP_NO_NETWORK);
                }*/
            }
            break;
        }
        else if(ind[i] == ',')
            pos--;
    }

    /* get "callheld" indicator*/
    pos = p_conn->callheld_ind_id -1;
    for(i=0; i< strlen(ind) ; i++)
    {
        if(!pos)
        {
            p_conn->curr_callheld_ind = ind[i] - '0';
            break;
        }
        else if(ind[i] == ',')
            pos--;
    }

    /* get "signal" indicator*/
    pos = p_conn->signal_strength_ind_id -1;
    for(i=0; i< strlen(ind) ; i++)
    {
        if(!pos)
        {
            p_conn->curr_signal_strength_ind = ind[i] - '0';
            break;
        }
        else if(ind[i] == ',')
            pos--;
    }

    /* get "roam" indicator*/
    pos = p_conn->roam_ind_id -1;
    for(i=0; i< strlen(ind) ; i++)
    {
        if(!pos)
        {
            p_conn->curr_roam_ind = ind[i] - '0';
            break;
        }
        else if(ind[i] == ',')
            pos--;
    }

    /* get "battchg" indicator*/
    pos = p_conn->battery_ind_id -1;
    for(i=0; i< strlen(ind) ; i++)
    {
        if(!pos)
        {
            p_conn->curr_battery_ind = ind[i] - '0';
            break;
        }
        else if(ind[i] == ',')
            pos--;
    }

    if(p_conn->curr_callheld_ind != 0)
    {
        BSA_HS_SETSTATUS(p_conn, BSA_HS_ST_3WAY_HELD);
    }
    else if(p_conn->curr_call_ind == BSA_HS_CALL_ACTIVE)
    {
        if(p_conn->curr_call_setup_ind == BSA_HS_CALLSETUP_INCOMING)
        {
            BSA_HS_SETSTATUS(p_conn, BSA_HS_ST_WAITCALL);
        }
        else
        {
            BSA_HS_SETSTATUS(p_conn, BSA_HS_ST_CALLACTIVE);
        }
    }
    else if(p_conn->curr_call_setup_ind == BSA_HS_CALLSETUP_INCOMING)
    {
        BSA_HS_SETSTATUS(p_conn, BSA_HS_ST_RINGACT);
    }
    else if((p_conn->curr_call_setup_ind == BSA_HS_CALLSETUP_OUTGOING) ||
            (p_conn->curr_call_setup_ind == BSA_HS_CALLSETUP_ALERTING)
           )
    {
        BSA_HS_SETSTATUS(p_conn, BSA_HS_ST_OUTGOINGCALL);
    }

    /* Dump indicators */
    if(p_conn->curr_service_ind < 2)
    APPL_TRACE_EVENT2("Service: %s,%d", app_hs_service_ind_name[p_conn->curr_service_ind],p_conn->curr_service_ind);

    if(p_conn->curr_call_ind < 2)
    APPL_TRACE_EVENT2("Call: %s,%d", app_hs_call_ind_name[p_conn->curr_call_ind],p_conn->curr_call_ind);

    if(p_conn->curr_call_setup_ind < 4)
    APPL_TRACE_EVENT2("Callsetup: Ind %s,%d", app_hs_callsetup_ind_name[p_conn->curr_call_setup_ind],p_conn->curr_call_setup_ind);

    if(p_conn->curr_callheld_ind < 3)
    APPL_TRACE_EVENT2("Hold: %s,%d", app_hs_callheld_ind_name[p_conn->curr_callheld_ind],p_conn->curr_callheld_ind);

    if(p_conn->curr_roam_ind < 2)
    APPL_TRACE_EVENT2("Roam: %s,%d", app_hs_roam_ind_name[p_conn->curr_roam_ind],p_conn->curr_roam_ind);
}

/*******************************************************************************
 **
 ** Function         app_hs_write_to_file
 **
 ** Description      write SCO IN data to file
 **
 ** Parameters
 **
 ** Returns          number of byte written.
 **
 *******************************************************************************/
static int app_hs_write_to_file(UINT8 *p_buf, int size)
{
#ifdef PCM_ALSA
    APP_ERROR0("Cannot write to file when PCM_ALSA is defined");
    return -1;
#endif /* PCM_ALSA */

    int ret;
    if(app_hs_cb.rec_fd <= 0)
    {
        APP_DEBUG0("no file to write...\n");
        return 0;
    }

    ret = write(app_hs_cb.rec_fd, p_buf, size);

    if(ret != size)
    {
        APP_ERROR1("write failed with code %d, fd %d", ret, app_hs_cb.rec_fd);
    }


    return ret;
}

/*******************************************************************************
 **
 ** Function         app_hs_sco_uipc_cback
 **
 ** Description     uipc audio call back function.
 **
 ** Parameters
 **
 ** Returns          void
 **
 *******************************************************************************/
static void app_hs_sco_uipc_cback(BT_HDR *p_buf)
{
    UINT8 *pp = (UINT8 *)(p_buf + 1);
    UINT8 pkt_len;

#ifdef PCM_ALSA
#ifdef ADANIS_ALSA_WRAPPER
    tBTA_SERVICE_MASK what_service = BTA_HFP_HS_SERVICE_MASK;
    size_t frames_in, frames_out, in_max;
#endif
    snd_pcm_sframes_t alsa_frames;
    snd_pcm_sframes_t alsa_frames_expected;
#endif /* PCM_ALSA */

    if (p_buf == NULL) {
        return;
    }

    pkt_len = p_buf->len;

    if (app_hs_cb.rec_fd>0) {
        app_hs_write_to_file(pp, pkt_len);
    }
    
#ifdef PCM_ALSA
    /* Compute number of PCM samples (contained in pkt_len->len bytes) */
    /* Divide by the number of channel */
    alsa_frames_expected = pkt_len / APP_HS_CHANNEL_NB;
    alsa_frames_expected /= 2; /* 16 bits samples */

#ifdef ADANIS_ALSA_WRAPPER
    alsa_handle_playback = adanis_alsa_playback_handle(what_service);
    alsa_handle_capture  = adanis_alsa_capture_handle(what_service);

    if(!alsa_handle_playback || !alsa_handle_capture) {
        APP_DEBUG0("BTA_HFP_HS_SERVICE_MASK NOT ACTIVATED !!");
        GKI_freebuf(p_buf);
        return;
    }
    
    adanis_alsa_mutex_lock();
#endif
    if (alsa_playback_opened != FALSE) {
        /*
        * Send PCM samples to ALSA/asound driver (local sound card)
        */
#ifdef ADANIS_ALSA_WRAPPER /* non blocking */
        alsa_frames = adanis_alsa_writebuf(alsa_handle_playback, pp, alsa_frames_expected, &frames_out);
#else
        alsa_frames = snd_pcm_writei(alsa_handle_playback, pp, alsa_frames_expected);
#endif
        if (alsa_frames < 0x00) {
            APP_DEBUG1("snd_pcm_recover %d", (int)alsa_frames);
            alsa_frames = snd_pcm_recover(alsa_handle_playback, alsa_frames, 0x00);
        }
        
        if (alsa_frames < 0x00) {
            APP_ERROR1("snd_pcm_writei failed: %s", snd_strerror(alsa_frames));
        }
        if (alsa_frames > 0x00 && alsa_frames < alsa_frames_expected) {
            APP_ERROR1("Short write (expected %d, wrote %d)",
                (int)alsa_frames_expected, (int)alsa_frames);
        }
    } else {
        APP_DEBUG0("alsa_playback_opened NOT");
    }

    if (alsa_capture_opened != FALSE) {
        /*
        * Read PCM samples from ALSA/asound driver (local sound card)
        */
#ifdef ADANIS_ALSA_WRAPPER /* non blocking */
        alsa_frames = adanis_alsa_readbuf(alsa_handle_capture, app_hs_cb.audio_buf, alsa_frames_expected, &frames_in, &in_max);
#else
        alsa_frames = snd_pcm_readi(alsa_handle_capture, app_hs_cb.audio_buf, alsa_frames_expected);
#endif
        if (alsa_frames < 0x00) {
            APP_ERROR1("snd_pcm_readi returns: %d", (int)alsa_frames);
#if 1 /* 2016/12/06 ADANIS YCJUNG ADD because pcm underrun occurred */
            alsa_frames = snd_pcm_recover(alsa_handle_capture, alsa_frames, 0x00);
#endif
        }
        else if ((alsa_frames > 0x00) &&
            (alsa_frames < alsa_frames_expected))
        {
            APP_ERROR1("Short read (expected %i, read %i)", (int)alsa_frames_expected, (int)alsa_frames);
        }
        /* Send them to UIPC (to Headet) */

        /* for now we just handle one instance */
        /* for multiple instance user should be prompted */
        if(alsa_frames > 0x00) { /* 2016/12/06 ADANIS YCJUNG ADD IF BLOCK */
            tBSA_HS_CONN_CB * p_conn = &(app_hs_cb.conn_cb[0x00]);

            if (TRUE != UIPC_Send(p_conn->uipc_channel, 0x00, (UINT8 *) app_hs_cb.audio_buf, alsa_frames * 0x02)) {
                APP_ERROR0("UIPC_Send failed");
            }
        }
    } else {
        APP_DEBUG0("alsa_capture NOT opened");
    }
#ifdef ADANIS_ALSA_WRAPPER
    adanis_alsa_mutex_unlock();
#endif
#endif /* PCM_ALSA */
    GKI_freebuf(p_buf);
}


/*******************************************************************************
 **
 ** Function         app_hs_read_xml_remote_devices
 **
 ** Description      This function is used to read the XML bluetooth remote device file
 **
 ** Parameters
 **
 ** Returns          void
 **
 *******************************************************************************/
int app_hs_read_xml_remote_devices(void)
{
    int status;
    int index;

    for (index = 0; index < APP_NUM_ELEMENTS(app_xml_remote_devices_db); index++)
    {
        app_xml_remote_devices_db[index].in_use = FALSE;
    }

    status = app_xml_read_db(APP_HS_XML_REM_DEVICES_FILE_PATH, app_xml_remote_devices_db,
            APP_NUM_ELEMENTS(app_xml_remote_devices_db));

    if (status < 0)
    {
        APP_ERROR1("app_xml_read_db failed (%d)", status);
        return -1;
    }
    return 0;
}

/* public function */

/*******************************************************************************
**
** Function         app_hs_open
**
** Description      Establishes mono headset connections
**
** Parameter        BD address to connect to. If its NULL, the app will prompt user for device.
**
** Returns          0 if success -1 if failure
*******************************************************************************/
int app_hs_open(BD_ADDR *bd_addr_in /*= NULL*/)
{
    tBSA_STATUS status = 0;
    BD_ADDR bd_addr;
    int device_index;

    tBSA_HS_OPEN param;

    APP_DEBUG0("Entering");

    if(bd_addr_in == NULL)
    {
        printf("Bluetooth AG menu:\n");
        printf("    0 Device from XML database (already paired)\n");
        printf("    1 Device found in last discovery\n");
        device_index = app_get_choice("Select source");
        /* Devices from XML databased */
        if (device_index == 0)
        {
            /* Read the Remote device xml file to have a fresh view */
            app_hs_read_xml_remote_devices();

            app_xml_display_devices(app_xml_remote_devices_db, APP_NUM_ELEMENTS(app_xml_remote_devices_db));
            device_index = app_get_choice("Select device");
            if ((device_index >= 0) &&
                (device_index < APP_NUM_ELEMENTS(app_xml_remote_devices_db)) &&
                (app_xml_remote_devices_db[device_index].in_use != FALSE))
            {
                bdcpy(bd_addr, app_xml_remote_devices_db[device_index].bd_addr);
            }
            else
            {
                printf("Bad Device Index:%d\n", device_index);
                return -1;
            }
        }
        /* Devices from Discovery */
        else
        {
            app_disc_display_devices();
            printf("Enter device number\n");
            device_index = app_get_choice("Select device");
            if ((device_index >= 0) &&
                (device_index < APP_DISC_NB_DEVICES) &&
                (app_discovery_cb.devs[device_index].in_use != FALSE))
            {
                bdcpy(bd_addr, app_discovery_cb.devs[device_index].device.bd_addr);
            }
            else
            {
                printf("Bad Device Index:%d\n", device_index);
                return -1;
            }
        }
    }
    else
    {
        bdcpy(bd_addr, *bd_addr_in);
    }

    BSA_HsOpenInit(&param);

    bdcpy(param.bd_addr, bd_addr);
    /* we manage only one connection for now */
    param.hndl = app_hs_cb.conn_cb[0].handle;
    status = BSA_HsOpen(&param);
    app_hs_cb.open_pending = TRUE;

    if (status != BSA_SUCCESS)
    {
        APP_ERROR1("BSA_HsOpen failed (%d)", status);
        app_hs_cb.open_pending = FALSE;
    }
    return status;
}

/*******************************************************************************
**
** Function         app_hs_audio_open
**
** Description      Open the SCO connection alone
**
** Parameter        None
**
** Returns          0 if success -1 if failure
*******************************************************************************/

int app_hs_audio_open(void)
{
    printf("app_hs_audio_open\n");

    tBSA_STATUS status;
    tBSA_HS_AUDIO_OPEN audio_open;
    tBSA_HS_CONN_CB * p_conn = &(app_hs_cb.conn_cb[0]);

    if(app_hs_cb.sco_route == BSA_SCO_ROUTE_HCI &&
        p_conn->uipc_channel == UIPC_CH_ID_BAD)
    {
        APP_ERROR0("Bad UIPC channel in app_hs_audio_open");
        return -1;
    }

    BSA_HsAudioOpenInit(&audio_open);
    audio_open.sco_route = app_hs_cb.sco_route;
    audio_open.hndl = app_hs_cb.conn_cb[0].handle;
    status = BSA_HsAudioOpen(&audio_open);
    if (status != BSA_SUCCESS)
    {
        APP_ERROR1("failed with status : %d", status);
        return -1;
    }
    return status;
}

/*******************************************************************************
**
** Function         app_hs_audio_close
**
** Description      Close the SCO connection alone
**
** Parameter        None
**
** Returns          0 if success -1 if failure
*******************************************************************************/

int app_hs_audio_close(void)
{
    printf("app_hs_audio_close\n");
    tBSA_STATUS status;
    tBSA_HS_AUDIO_CLOSE audio_close;
    BSA_HsAudioCloseInit(&audio_close);
    audio_close.hndl = app_hs_cb.conn_cb[0].handle;
    status = BSA_HsAudioClose(&audio_close);
    if (status != BSA_SUCCESS)
    {
        APP_ERROR1("failed with status : %d", status);
        return -1;
    }
    return status;
}

/*******************************************************************************
**
** Function         app_hs_close
**
** Description      release mono headset connections
**
** Returns          0 if success -1 if failure
*******************************************************************************/
int app_hs_close(void)
{
    tBSA_HS_CLOSE param;
    tBSA_STATUS status;

    /* Prepare parameters */
    BSA_HsCloseInit(&param);

    /* todo : promt user to check what to close here */
    /* for now we just handle one AG at a time */
    param.hndl = app_hs_cb.conn_cb[0].handle;

    status = BSA_HsClose(&param);
    if (status != BSA_SUCCESS)
    {
        APP_ERROR1("failed with status : %d", status);
        return -1;
    }

    return 0;
}

/*******************************************************************************
**
** Function         app_hs_cancel
**
** Description      cancel connections
**
** Returns          0 if success -1 if failure
*******************************************************************************/
int app_hs_cancel(void)
{
    tBSA_HS_CANCEL param;
    tBSA_STATUS status;

    /* Prepare parameters */
    BSA_HsCancelInit(&param);

    status = BSA_HsCancel(&param);
    if (status != BSA_SUCCESS)
    {
        APP_ERROR1("failed with status : %d", status);
        return -1;
    }

    return 0;
}

/*******************************************************************************
**
** Function         app_is_open_pending
**
**
** Returns          TRUE if open if pending
*******************************************************************************/
BOOLEAN app_hs_is_open_pending(BD_ADDR bda_open_pending)
{
    bdcpy(bda_open_pending, app_hs_cb.open_pending_bda);
    return app_hs_cb.open_pending;
}

/*******************************************************************************
**
** Function         app_hs_answer_call
**
** Description      example of function to answer the call
**
** Returns          0 if success -1 if failure
*******************************************************************************/
int app_hs_answer_call(void)
{
    tBSA_HS_COMMAND cmd_param;
    tBSA_HS_CONN_CB *p_conn;

    printf("app_hs_answer_call\n");

    /* If no connection exist, error */
    if ((p_conn = app_hs_get_default_conn()) == NULL)
    {
        APP_DEBUG0("no connection available");
        return -1;
    }

    BSA_HsCommandInit(&cmd_param);
    cmd_param.hndl = p_conn->handle;
    cmd_param.command = BSA_HS_A_CMD;
    BSA_HsCommand(&cmd_param);
    return 0;
}

/*******************************************************************************
**
** Function         app_hs_hangup
**
** Description      example of function to hang up
**
** Returns          0 if success -1 if failure
*******************************************************************************/
int app_hs_hangup(void)
{
    tBSA_HS_COMMAND cmd_param;
    tBSA_HS_CONN_CB *p_conn;

    printf("app_hs_hangup\n");

    /* If no connection exist, error */
    if ((p_conn = app_hs_get_default_conn()) == NULL)
    {
        APP_DEBUG0("no connection available");
        return -1;
    }

    BSA_HsCommandInit(&cmd_param);
    cmd_param.hndl = p_conn->handle;
    cmd_param.command = BSA_HS_CHUP_CMD;
    BSA_HsCommand(&cmd_param);

    return 0;
}

/*******************************************************************************
**
** Function         app_hs_stop
**
** Description      This function is used to stop hs services and close all
**                  UIPC channel.
**
** Parameters       void
**
** Returns          void
**
*******************************************************************************/
void app_hs_stop(void)
{
    tBSA_HS_DISABLE      disable_param;
    tBSA_HS_DEREGISTER   deregister_param;
    tBSA_HS_CONN_CB * p_conn;
    UINT8 index;

    BSA_HsDeregisterInit(&deregister_param);
    for(index=0; index<BSA_HS_MAX_NUM_CONN; index++)
    {
        p_conn = &(app_hs_cb.conn_cb[index]);
        deregister_param.hndl = p_conn->handle;
        APP_DEBUG1("handle %d", deregister_param.hndl);
        BSA_HsDeregister(&deregister_param);

        if(p_conn->uipc_channel != UIPC_CH_ID_BAD)
        {
            if(p_conn->uipc_connected)
            {
                APPL_TRACE_DEBUG0("Closing UIPC Channel");
                UIPC_Close(p_conn->uipc_channel);
                p_conn->uipc_connected = FALSE;
            }
            p_conn->uipc_channel = UIPC_CH_ID_BAD;
        }
    }

    BSA_HsDisableInit(&disable_param);
    BSA_HsDisable(&disable_param);

    /* for now we just handle one instance */
    /* for multiple instance user should be prompted */
}

/*******************************************************************************
**
** Function         app_hs_close_rec_file
**
** Description      Close recording file
**
** Parameters       void
**
** Returns          void
**
*******************************************************************************/
void app_hs_close_rec_file(void)
{
    int fd;
    tAPP_WAV_FILE_FORMAT format;

    if(app_hs_cb.rec_fd <= 0)
        return;

    format.bits_per_sample = 8;
    format.bits_per_sample = APP_HS_BITS_PER_SAMPLE;
    format.nb_channels = APP_HS_CHANNEL_NB;
    format.sample_rate = APP_HS_SAMPLE_RATE;

    fd = app_hs_cb.rec_fd;
    app_hs_cb.rec_fd = 0;
    app_wav_close_file(fd, &format);
}

/*******************************************************************************
**
** Function         app_hs_open_rec_file
**
** Description     Open recording file
**
** Parameters      filename to open
**
** Returns          void
**
*******************************************************************************/
void app_hs_open_rec_file(char * filename)
{
    app_hs_cb.rec_fd = app_wav_create_file(filename, 0);
}

/*******************************************************************************
**
** Function         app_hs_play_file
**
** Description      Play SCO data from file to SCO OUT channel
**
** Parameters       char * filename
**
** Returns          0 if success -1 if failure
**
*******************************************************************************/
int app_hs_play_file(char * filename)
{
    tAPP_WAV_FILE_FORMAT  wav_format;
    int nb_bytes = 0;
    int fd = 0;

    printf("app_hs_play_file\n");

    fd = app_wav_open_file(filename, &wav_format);

    if(fd < 0)
    {
        printf("Error could not open wav input file\n");
        printf("Use the Record audio file function to create an audio file called %s and then try again\n",APP_HS_SCO_OUT_SOUND_FILE);
        return -1;
    }

    do
    {
        nb_bytes = read(fd, app_hs_cb.audio_buf, sizeof(app_hs_cb.audio_buf)); /* read audio sample */

        if(nb_bytes < 0)
        {
            close(fd);
            return -1;
        }

        /* for now we just handle one instance */
        /* for multiple instance user should be prompted */
        tBSA_HS_CONN_CB * p_conn = &(app_hs_cb.conn_cb[0]);

        if (TRUE != UIPC_Send(p_conn->uipc_channel, 0,
                (UINT8 *) app_hs_cb.audio_buf,
                nb_bytes))
        {
            printf("error in UIPC send could not send data \n");
        }

    } while (nb_bytes != 0);

    close(fd);

    return 0;
}

/*******************************************************************************
**
** Function         app_hs_cback
**
** Description      Example of HS callback function
**
** Parameters       event code and data
**
** Returns          void
**
*******************************************************************************/
void app_hs_cback(tBSA_HS_EVT event, tBSA_HS_MSG *p_data)
{
    char buf[100] = {0,};
    UINT16 handle = 0;
    tBSA_HS_CONN_CB *p_conn;

    if (!p_data) {
        printf("app_hs_cback p_data=NULL for event:%d\n", event);
        return;
    }

    /* retrieve the handle of the connection for which this event */
    handle = p_data->hdr.handle;
    APPL_TRACE_DEBUG2("app_hs_cback event:%d for handle: %d", event, handle);

    /* retrieve the connection for this handle */
    p_conn = app_hs_get_conn_by_handle(handle);

    if (!p_conn) {
        printf("app_hs_cback: handle %d not supported\n", handle);
        return;
    }

    switch (event)
    {
    case BSA_HS_CONN_EVT:       /* Service level connection */
        printf("BSA_HS_CONN_EVT:\n");
        app_hs_cb.open_pending = FALSE;
        printf("    - Remote bdaddr: %02x:%02x:%02x:%02x:%02x:%02x\n",
                p_data->conn.bd_addr[0], p_data->conn.bd_addr[1],
                p_data->conn.bd_addr[2], p_data->conn.bd_addr[3],
                p_data->conn.bd_addr[4], p_data->conn.bd_addr[5]);
        printf("    - Service: ");
        switch (p_data->conn.service)
        {
        case BSA_HSP_HS_SERVICE_ID:
            printf("Headset\n");
            break;
        case BSA_HFP_HS_SERVICE_ID:
            printf("Handsfree\n");
            break;
        default:
            printf("Not supported 0x%08x\n", p_data->conn.service);
            return;
            break;
        }

        /* check if this conneciton is already opened */
        if (p_conn->connection_active)
        {
            printf("BSA_HS_CONN_EVT: connection already opened for handle %d\n", handle);
            break;
        }
        bdcpy(p_conn->connected_bd_addr, p_data->conn.bd_addr);
        p_conn->handle = p_data->conn.handle;
        p_conn->connection_active = TRUE;
        p_conn->connected_hs_service_id = p_data->conn.service;
        p_conn->peer_feature = p_data->conn.peer_features;
        p_conn->status = BSA_HS_ST_CONNECT;
#if 1 /* ADANIS YCJUNG */
        /* reset */
        playback_rate = 8000;
#endif
        break;


    case BSA_HS_CLOSE_EVT:      /* Connection Closed (for info)*/
        /* Close event, reason BSA_HS_CLOSE_CLOSED or BSA_HS_CLOSE_CONN_LOSS */
        APP_DEBUG1("BSA_HS_CLOSE_EVT, reason %d", p_data->hdr.status);
        app_hs_cb.open_pending = FALSE;

        if (!p_conn->connection_active)
        {
            printf("BSA_HS_CLOSE_EVT: connection not opened for handle %d\n", handle);
            break;
        }
        p_conn->connection_active = FALSE;
        p_conn->indicator_string_received = FALSE;


        BSA_HS_SETSTATUS(p_conn, BSA_HS_ST_CONNECTABLE);
        break;

    case BSA_HS_AUDIO_OPEN_EVT:     /* Audio Open Event */
        printf("BSA_HS_AUDIO_OPEN_EVT : %d\n", p_conn->uipc_channel);
        if(app_hs_cb.sco_route == BSA_SCO_ROUTE_HCI &&
           p_conn->uipc_channel != UIPC_CH_ID_BAD &&
           !p_conn->uipc_connected) {
            /* Open UIPC channel for TX channel ID */
            if(UIPC_Open(p_conn->uipc_channel, app_hs_sco_uipc_cback)!= TRUE) {
                APP_ERROR1("app_hs_register failed to open UIPC channel(%d)",
                        p_conn->uipc_channel);
                break;
            }
            p_conn->uipc_connected = TRUE;
            UIPC_Ioctl(p_conn->uipc_channel,UIPC_REG_CBACK,app_hs_sco_uipc_cback);
        }
#ifdef PCM_ALSA
        if(BSA_HS_GETSTATUS(p_conn, BSA_HS_ST_SCOOPEN))
            app_hs_close_alsa_duplex();
        app_hs_open_alsa_duplex();
#endif /* PCM_ALSA */

        p_conn->call_state = BSA_HS_CALL_CONN;
        BSA_HS_SETSTATUS(p_conn, BSA_HS_ST_SCOOPEN);
        break;

    case BSA_HS_AUDIO_CLOSE_EVT:         /* Audio Close event */
        fprintf(stdout,"BSA_HS_AUDIO_CLOSE_EVT\n");
#ifdef PCM_ALSA
        app_hs_close_alsa_duplex();
#endif /* PCM_ALSA */

        if (!p_conn->connection_active) {
            printf("BSA_HS_AUDIO_CLOSE_EVT: connection not opened for handle %d\n", handle);
            return;
        }
        p_conn->call_state = BSA_HS_CALL_NONE;
        BSA_HS_RESETSTATUS(p_conn, BSA_HS_ST_SCOOPEN);

        if(p_conn->uipc_channel != UIPC_CH_ID_BAD &&
           p_conn->uipc_connected)
        {
            APPL_TRACE_DEBUG0("Closing UIPC Channel");
            UIPC_Close(p_conn->uipc_channel);
            p_conn->uipc_connected = FALSE;
        }
        break;

    case BSA_HS_CIEV_EVT:                /* CIEV event */
        printf("BSA_HS_CIEV_EVT\n");
        strncpy(buf, p_data->val.str, 4);
        buf[5] ='\0';
        printf("Call Ind Status %s\n",buf);
        break;

    case BSA_HS_CIND_EVT:                /* CIND event */
        printf("BSA_HS_CIND_EVT\n");
        printf("Call Indicator %s\n",p_data->val.str);

        /* check if indicator configuration was received */
        if(p_conn->indicator_string_received)
        {
            app_hs_set_initial_indicator_status(p_conn, p_data->val.str);
        }
        else
        {
            p_conn->indicator_string_received = TRUE;
            app_hs_decode_indicator_string(p_conn, p_data->val.str);
        }
        break;

    case BSA_HS_RING_EVT:
        fprintf(stdout, "BSA_HS_RING_EVT\n");
        break;

    case BSA_HS_CLIP_EVT:
        fprintf(stdout, "BSA_HS_CLIP_EVT\n");
        break;

    case BSA_HS_BSIR_EVT:
        fprintf(stdout, "BSA_HS_BSIR_EVT\n");
        break;

    case BSA_HS_BVRA_EVT:
        fprintf(stdout, "BSA_HS_BVRA_EVT\n");
        break;

    case BSA_HS_CCWA_EVT:
        fprintf(stdout, "Call waiting : BSA_HS_CCWA_EVT:%s\n", p_data->val.str);
        break;

    case BSA_HS_CHLD_EVT:
        fprintf(stdout, "BSA_HS_CHLD_EVT\n");
        break;

    case BSA_HS_VGM_EVT:
        fprintf(stdout, "BSA_HS_VGM_EVT\n");
        break;

    case BSA_HS_VGS_EVT:
        fprintf(stdout, "BSA_HS_VGS_EVT\n");
        break;

    case BSA_HS_BINP_EVT:
        fprintf(stdout, "BSA_HS_BINP_EVT\n");
        break;

    case BSA_HS_BTRH_EVT:
        fprintf(stdout, "BSA_HS_BTRH_EVT\n");
        break;

    case BSA_HS_CNUM_EVT:
        fprintf(stdout, "BSA_HS_CNUM_EVT:%s\n",p_data->val.str);
        break;

    case BSA_HS_COPS_EVT:
        fprintf(stdout, "BSA_HS_COPS_EVT:%s\n",p_data->val.str);
        break;

    case BSA_HS_CMEE_EVT:
        fprintf(stdout, "BSA_HS_CMEE_EVT:%s\n", p_data->val.str);
        break;

    case BSA_HS_CLCC_EVT:
        fprintf(stdout, "BSA_HS_CLCC_EVT:%s\n", p_data->val.str);
        break;

    case BSA_HS_UNAT_EVT:
        fprintf(stdout, "BSA_HS_UNAT_EVT\n");
        break;

    case BSA_HS_OK_EVT:
        fprintf(stdout, "BSA_HS_OK_EVT: command value %d, %s\n",p_data->val.num, p_data->val.str);
        break;

    case BSA_HS_ERROR_EVT:
        fprintf(stdout, "BSA_HS_ERROR_EVT\n");
        break;

    case BSA_HS_BCS_EVT:
        fprintf(stdout, "BSA_HS_BCS_EVT: codec %d (%s)\n",p_data->val.num,
            (p_data->val.num == BSA_SCO_CODEC_MSBC) ? "mSBC":"CVSD");
#if 1 /* ADANIS YCJUNG */
        if(p_data->val.num == BSA_SCO_CODEC_MSBC) {
            playback_rate      = 16000;
        } else {
            playback_rate      =  8000;
        }
#endif
        break;

    case BSA_HS_OPEN_EVT:
        fprintf(stdout, "BSA_HS_OPEN_EVT\n");
        app_hs_cb.open_pending = FALSE;
        break;

    default:
        printf("app_hs_cback unknown event:%d\n", event);
        break;
    }
    fflush(stdout);

    /* forward callback to the registered application */
    if(s_pHsCallback)
        s_pHsCallback(event, p_data);
}

/*******************************************************************************
**
** Function         app_hs_start
**
** Description      Example of function to start the Headset application
**
** Parameters		Callback for event notification (can be NULL, if NULL default will be used)
**                      sco_route SCO route path(BSA_SCO_ROUTE_PCM or BSA_SCO_ROUTE_HCI) ** ADANIS YCJUNG **
**
** Returns          0 if ok -1 in case of error
**
*******************************************************************************/
int app_hs_start(tHsCallback cb, UINT8 sco_route)
{
    tBSA_STATUS status;

    status = app_hs_enable();
    if (status != 0)
    {
        return status;
    }

    status = app_hs_register(sco_route);

    s_pHsCallback = cb;

    return status;
}

/*******************************************************************************
**
** Function         app_hs_enable
**
** Description      Example of function to start enable Hs service
**
** Parameters       void
**
** Returns          0 if ok -1 in case of error
**
*******************************************************************************/
int app_hs_enable(void)
{
    int                status = 0;
    tBSA_HS_ENABLE     enable_param;

    /* prepare parameters */
    BSA_HsEnableInit(&enable_param);
    enable_param.p_cback = app_hs_cback;

    status = BSA_HsEnable(&enable_param);
    if (status != BSA_SUCCESS)
    {
        APP_DEBUG1("BSA_HsEnable failes with status %d", status);
        return -1;
    }
    return 0;
}

/*******************************************************************************
**
** Function         app_hs_register
**
** Description      Example of function to start register one Hs instance
**
** Parameters       sco_route SCO route path(BSA_SCO_ROUTE_PCM or BSA_SCO_ROUTE_HCI) ** ADANIS YCJUNG **
**
** Returns          0 if ok -1 in case of error
**
*******************************************************************************/
int app_hs_register(UINT8 sco_route)
{
    int index, status = 0;
    tBSA_HS_REGISTER   param;
    tBSA_HS_CONN_CB * p_conn;

#ifdef PCM_ALSA
#if defined( ADANIS_ALSA_LATENCY ) || defined( ADANIS_ALSA_WRAPPER )
    if(sco_route == BSA_SCO_ROUTE_PCM) {
        status = adanis_i2s_init_thread_alsa_SCO();
        if(status == FALSE) {
            APP_ERROR0("[hs] change sco_route PCM to HCI !!");
            sco_route = BSA_SCO_ROUTE_HCI;
        }

        app_hs_cb.audio_I2S_SCO_looping = FALSE;
    }
#endif
#endif

#if 1 /* ADANIS YCJUNG */
    app_hs_cb.sco_route = sco_route;
#else
    app_hs_cb.sco_route = BSA_SCO_ROUTE_DEFAULT;
#endif

    APP_DEBUG1("start Register sco_route[%d]", sco_route/*BSA_SCO_ROUTE_DEFAULT*/);

    /* prepare parameters */
    BSA_HsRegisterInit(&param);
    param.services = BSA_HSP_HS_SERVICE_MASK | BSA_HFP_HS_SERVICE_MASK;
    param.sec_mask = BSA_SEC_NONE;
    /* Use BSA_HS_FEAT_CODEC (for WBS) for SCO over PCM only, not for SCO over HCI*/
    param.features = APP_HS_FEATURES;
    if(app_hs_cb.sco_route == BSA_SCO_ROUTE_HCI)
        param.features &= ~BSA_HS_FEAT_CODEC;
    param.settings.ecnr_enabled = (param.features & BSA_HS_FEAT_ECNR) ? TRUE : FALSE;
    param.settings.mic_vol = APP_HS_MIC_VOL;
    param.settings.spk_vol = APP_HS_SPK_VOL;
    strncpy(param.service_name[0], APP_HS_HSP_SERVICE_NAME, BSA_HS_SERVICE_NAME_LEN_MAX);
    strncpy(param.service_name[1], APP_HS_HFP_SERVICE_NAME, BSA_HS_SERVICE_NAME_LEN_MAX);

    /* SCO routing options:  BSA_SCO_ROUTE_HCI or BSA_SCO_ROUTE_PCM */
    param.sco_route = app_hs_cb.sco_route;

    for(index=0; index<BSA_HS_MAX_NUM_CONN; index++)
    {
        status = BSA_HsRegister(&param);
        if (status != BSA_SUCCESS)
        {
            APP_ERROR1("Unable to register HS with status %d", status);
            return -1;
        }

        if (status != BSA_SUCCESS)
        {
            APP_ERROR1("BSA_HsRegister failed(%d)", status);
            return -1;
        }

        p_conn = &(app_hs_cb.conn_cb[index]);
        p_conn->handle = param.hndl;
        p_conn->uipc_channel = param.uipc_channel;

        APP_DEBUG1("Register complete handle %d, status %d, uipc [%d->%d], sco_route %d",
                param.hndl, status, p_conn->uipc_channel, param.uipc_channel, param.sco_route);
    }

    APP_DEBUG0("Register complete");

    return 0;
}

/*******************************************************************************
 **
 ** Function         app_hs_init
 **
 ** Description      Init Headset application
 **
 ** Parameters
 **
 ** Returns          0 if successful execution, error code else
 **
 *******************************************************************************/
void app_hs_init(void)
{
    UINT8 index;

    memset(&app_hs_cb, 0, sizeof(app_hs_cb));

    for(index=0; index<BSA_HS_MAX_NUM_CONN ; index++)
    {
        app_hs_cb.conn_cb[index].uipc_channel = UIPC_CH_ID_BAD;
    }

#ifdef PCM_ALSA
#ifdef ADANIS_ALSA_WRAPPER
    app_hs_cb.audio_dummy = 0xADAD;
#endif
#endif
}

/*******************************************************************************
**
** Function         app_hs_hold_call
**
** Description      Hold active call
**
** Parameters       void
**
** Returns          0 if successful execution, error code else
**
*******************************************************************************/
int app_hs_hold_call(tBSA_BTHF_CHLD_TYPE_T type)
{
    tBSA_HS_COMMAND cmd_param;
    tBSA_HS_CONN_CB *p_conn;

    printf("app_hs_hold_call\n");

    /* If no connection exist, error */
    if ((p_conn = app_hs_get_default_conn()) == NULL)
    {
        APP_DEBUG0("no connection available");
        return -1;
    }

    BSA_HsCommandInit(&cmd_param);
    cmd_param.hndl = p_conn->handle;
    cmd_param.command = BSA_HS_CHLD_CMD;
    cmd_param.data.num = type;
    BSA_HsCommand(&cmd_param);
    return 0;
}

/*******************************************************************************
**
** Function         app_hs_last_num_dial
**
** Description      Re-dial last dialed number
**
** Parameters       void
**
** Returns          0 if successful execution, error code else
**
*******************************************************************************/
int app_hs_last_num_dial(void)
{
    tBSA_HS_COMMAND cmd_param;
    tBSA_HS_CONN_CB *p_conn;

    printf("app_hs_answer_call\n");

    /* If no connection exist, error */
    if ((p_conn = app_hs_get_default_conn()) == NULL)
    {
        APP_DEBUG0("no connection available");
        return -1;
    }

    BSA_HsCommandInit(&cmd_param);
    cmd_param.hndl = p_conn->handle;
    cmd_param.command = BSA_HS_BLDN_CMD;

    BSA_HsCommand(&cmd_param);
    return 0;
}

/*******************************************************************************
**
** Function         app_hs_dial_num
**
** Description      Dial a phone number
**
** Parameters       Phone number string
**
** Returns          0 if successful execution, error code else
**
*******************************************************************************/
int app_hs_dial_num(const char *num)
{
    tBSA_HS_COMMAND cmd_param;
    tBSA_HS_CONN_CB *p_conn;

    printf("app_hs_answer_call\n");

    if((num == NULL) || (strlen(num) == 0))
    {
        APP_DEBUG0("empty number string");
        return -1;
    }

    /* If no connection exist, error */
    if ((p_conn = app_hs_get_default_conn()) == NULL)
    {
        APP_DEBUG0("no connection available");
        return -1;
    }

    BSA_HsCommandInit(&cmd_param);
    cmd_param.hndl = p_conn->handle;
    cmd_param.command = BSA_HS_D_CMD;

    strcpy(cmd_param.data.str, num);
    strcat(cmd_param.data.str, ";");
    BSA_HsCommand(&cmd_param);
    return 0;
}

/*******************************************************************************
**
** Function         app_hs_send_unat
**
** Description      Send an unknown AT Command
**
** Parameters       char *cCmd
**
** Returns          0 if successful execution, error code else
**
*******************************************************************************/
int app_hs_send_unat(char *cCmd)
{
    tBSA_HS_COMMAND cmd_param;
    tBSA_HS_CONN_CB *p_conn=NULL;

    printf("app_hs_send_unat:Command : %s\n", cCmd);

    /* If no connection exist, error */
    if ((NULL==(p_conn = app_hs_get_default_conn())) || NULL==cCmd)
    {
        APP_DEBUG0("no connection available or invalid AT Command");
        return -1;
    }

    BSA_HsCommandInit(&cmd_param);
    cmd_param.hndl = p_conn->handle;
    cmd_param.command = BSA_HS_UNAT_CMD;

    strncpy(cmd_param.data.str, cCmd, sizeof(cmd_param.data.str)-1);

    BSA_HsCommand(&cmd_param);
    return 0;
}

/*******************************************************************************
**
** Function         app_hs_send_clcc
**
** Description      Send CLCC AT Command for obtaining list of current calls
**
** Parameters       None
**
** Returns          0 if successful execution, error code else
**
*******************************************************************************/
int app_hs_send_clcc_cmd()
{
    printf("app_hs_send_clcc_cmd\n");
    tBSA_HS_COMMAND cmd_param;
    tBSA_HS_CONN_CB *p_conn=NULL;

    /* If no connection exist, error */
    if (NULL==(p_conn = app_hs_get_default_conn()))
    {
        APP_DEBUG0("no connection available");
        return -1;
    }

    BSA_HsCommandInit(&cmd_param);
    cmd_param.hndl = p_conn->handle;
    cmd_param.command = BSA_HS_CLCC_CMD;
    BSA_HsCommand(&cmd_param);
    return 0;
}

/*******************************************************************************
**
** Function         app_hs_send_cops_cmd
**
** Description      Send COPS AT Command to obtain network details and set network details
**
** Parameters       char *cCmd
**
** Returns          0 if successful execution, error code else
**
*******************************************************************************/
int app_hs_send_cops_cmd(char *cCmd)
{
    printf("app_hs_send_cops_cmd\n");
    tBSA_HS_COMMAND cmd_param;
    tBSA_HS_CONN_CB *p_conn=NULL;

    /* If no connection exist, error */
    if (NULL==(p_conn = app_hs_get_default_conn()))
    {
        APP_DEBUG0("no connection available");
        return -1;
    }

    BSA_HsCommandInit(&cmd_param);
    cmd_param.hndl = p_conn->handle;
    strncpy(cmd_param.data.str, cCmd, sizeof(cmd_param.data.str)-1);
    cmd_param.command = BSA_HS_COPS_CMD;
    BSA_HsCommand(&cmd_param);
    return 0;
}

/*******************************************************************************
**
** Function         app_hs_send_ind_cmd
**
** Description      Send indicator (CIND) AT Command
**
** Parameters       None
**
** Returns          0 if successful execution, error code else
**
*******************************************************************************/
int app_hs_send_ind_cmd()
{
    printf("app_hs_send_cind_cmd\n");
    tBSA_HS_COMMAND cmd_param;
    tBSA_HS_CONN_CB *p_conn=NULL;

    /* If no connection exist, error */
    if (NULL==(p_conn = app_hs_get_default_conn()))
    {
        APP_DEBUG0("no connection available");
        return -1;
    }

    BSA_HsCommandInit(&cmd_param);
    cmd_param.hndl = p_conn->handle;
    cmd_param.command = BSA_HS_CIND_CMD;
    BSA_HsCommand(&cmd_param);
    return 0;
}

/*******************************************************************************
**
** Function         app_hs_mute_unmute_microphone
**
** Description      Send Mute / unmute commands
**
** Returns          Returns SUCCESS or FAILURE on sending mute/unmute command
*******************************************************************************/
int app_hs_mute_unmute_microphone(BOOLEAN bMute)
{
    char str[20];
    memset(str,'\0',sizeof(str));
    strcpy(str, "+CMUT=");

    if(bMute)
        strcat(str, "1");
    else
        strcat(str, "0");
    return app_hs_send_unat(str);
}

/*******************************************************************************
**
** Function         app_hs_send_dtmf
**
** Description      Send DTMF AT Command
**
** Parameters       char dtmf (0-9, #, A-D)
**
** Returns          0 if successful execution, error code else
**
*******************************************************************************/
int app_hs_send_dtmf(char dtmf)
{
    tBSA_HS_COMMAND cmd_param;
    tBSA_HS_CONN_CB *p_conn=NULL;

    printf("app_hs_send_dtmf:Command : %x\n", dtmf);

    /* If no connection exist, error */
    if ((NULL==(p_conn = app_hs_get_default_conn())) || '\0'==dtmf)
    {
        APP_DEBUG0("no connection available or invalid DTMF Command");
        return -1;
    }

    BSA_HsCommandInit(&cmd_param);
    cmd_param.hndl = p_conn->handle;
    cmd_param.command = BSA_HS_VTS_CMD;

    cmd_param.data.str[0] = dtmf;
    BSA_HsCommand(&cmd_param);
    return 0;
}

/*******************************************************************************
**
** Function         app_hs_send_cnum
**
** Description      Send CNUM AT Command
**
** Parameters       No parameter needed
**
** Returns          0 if successful execution, error code else
**
*******************************************************************************/
int app_hs_send_cnum(void)
{
    tBSA_HS_COMMAND cmd_param;
    tBSA_HS_CONN_CB *p_conn=NULL;

    printf("app_hs_send_cnum:Command \n");

    /* If no connection exist, error */
    if ((NULL==(p_conn = app_hs_get_default_conn())))
    {
        APP_DEBUG0("no connection available or invalid DTMF Command");
        return -1;
    }

    BSA_HsCommandInit(&cmd_param);
    cmd_param.hndl = p_conn->handle;
    cmd_param.command = BSA_HS_CNUM_CMD;
    cmd_param.data.num=0;

    BSA_HsCommand(&cmd_param);
    return 0;
}

/*******************************************************************************
**
** Function         app_hs_send_keypress_evt
**
** Description      Send Keypress event
**
** Parameters       char *cCmd - Keyboard sequence (0-9, * , #)
**
** Returns          0 if successful execution, error code else
**
*******************************************************************************/
int app_hs_send_keypress_evt(char *cCmd)
{
    tBSA_HS_COMMAND cmd_param;
    tBSA_HS_CONN_CB *p_conn=NULL;

    printf("app_hs_send_keypress_evt:Command \n");

    /* If no connection exist, error */
    if ((NULL==(p_conn = app_hs_get_default_conn())))
    {
        APP_DEBUG0("no connection available");
        return -1;
    }

    BSA_HsCommandInit(&cmd_param);
    cmd_param.hndl = p_conn->handle;
    cmd_param.command = BSA_HS_CKPD_CMD;
    strncpy(cmd_param.data.str, cCmd, sizeof(cmd_param.data.str)-1);

    BSA_HsCommand(&cmd_param);
    return 0;
}

/*******************************************************************************
**
** Function         app_hs_start_voice_recognition
**
** Description      Start voice recognition
**
** Parameters       None
**
** Returns          0 if successful execution, error code else
**
*******************************************************************************/
int app_hs_start_voice_recognition(void)
{
    tBSA_HS_COMMAND cmd_param;
    tBSA_HS_CONN_CB *p_conn=NULL;

    printf("app_hs_start_voice_recognition:Command \n");

    /* If no connection exist, error */
    if ((NULL==(p_conn = app_hs_get_default_conn())))
    {
        APP_DEBUG0("no connection available");
        return -1;
    }

    if(app_hs_cb.conn_cb[0].peer_feature & BSA_HS_PEER_FEAT_VREC)
    {
        BSA_HsCommandInit(&cmd_param);
        cmd_param.hndl = p_conn->handle;
        cmd_param.command = BSA_HS_BVRA_CMD;
        cmd_param.data.num = 1;

        BSA_HsCommand(&cmd_param);
        return 0;
    }

    APP_DEBUG0("Peer feature - VR feature not available");
    return -1;
}


/*******************************************************************************
**
** Function         app_hs_stop_voice_recognition
**
** Description      Stop voice recognition
**
** Parameters       None
**
** Returns          0 if successful execution, error code else
**
*******************************************************************************/
int app_hs_stop_voice_recognition(void)
{
    tBSA_HS_COMMAND cmd_param;
    tBSA_HS_CONN_CB *p_conn=NULL;

    printf("app_hs_stop_voice_recognition:Command \n");

    /* If no connection exist, error */
    if ((NULL==(p_conn = app_hs_get_default_conn())))
    {
        APP_DEBUG0("no connection available");
        return -1;
    }

    if(app_hs_cb.conn_cb[0].peer_feature & BSA_HS_PEER_FEAT_VREC)
    {
        BSA_HsCommandInit(&cmd_param);
        cmd_param.hndl = p_conn->handle;
        cmd_param.command = BSA_HS_BVRA_CMD;
        cmd_param.data.num = 0;
        BSA_HsCommand(&cmd_param);
        return 0;
   }

   APP_DEBUG0("Peer feature - VR feature not available");
   return -1;
}

/*******************************************************************************
**
** Function         app_hs_set_volume
**
** Description      Send volume AT Command
**
** Parameters       tBSA_BTHF_VOLUME_TYPE_T type, int volume
**
** Returns          0 if successful execution, error code else
**
*******************************************************************************/
int app_hs_set_volume(tBSA_BTHF_VOLUME_TYPE_T type, int volume)
{
    tBSA_HS_COMMAND cmd_param;
    tBSA_HS_CONN_CB *p_conn=NULL;

    printf("app_hs_set_volume:Command : %d, %d\n", type, volume);

    /* If no connection exist, error */
    if ((NULL==(p_conn = app_hs_get_default_conn())))
    {
        APP_DEBUG0("no connection available");
        return -1;
    }

    BSA_HsCommandInit(&cmd_param);
    cmd_param.hndl = p_conn->handle;

    if(BTHF_VOLUME_TYPE_SPK == type)
        cmd_param.command = BSA_HS_SPK_CMD;
    else
        cmd_param.command = BSA_HS_MIC_CMD;

    cmd_param.data.num = volume;
    BSA_HsCommand(&cmd_param);
    return 0;
}

/*******************************************************************************
**
** Function         app_hs_getallIndicatorValues
**
** Description      Get all indicator values
**
** Parameters       tBSA_HS_IND_VALS *pIndVals
**
** Returns          0 if successful execution, error code else
**
*******************************************************************************/
int app_hs_getallIndicatorValues(tBSA_HS_IND_VALS *pIndVals)
{
    tBSA_HS_CONN_CB * p_conn = &(app_hs_cb.conn_cb[0]);
    if(NULL!=p_conn && NULL!=pIndVals)
    {
       pIndVals->curr_callwait_ind = 0;
       pIndVals->curr_signal_strength_ind = p_conn->curr_signal_strength_ind;
       pIndVals->curr_battery_ind = p_conn->curr_battery_ind;
       pIndVals->curr_call_ind = p_conn->curr_call_ind;
       pIndVals->curr_call_setup_ind = p_conn->curr_call_setup_ind;
       pIndVals->curr_roam_ind = p_conn->curr_roam_ind;
       pIndVals->curr_service_ind = p_conn->curr_service_ind;
       pIndVals->curr_callheld_ind = p_conn->curr_callheld_ind;
       if(pIndVals->curr_call_ind == BSA_HS_CALL_ACTIVE &&
          pIndVals->curr_call_setup_ind == BSA_HS_CALLSETUP_INCOMING)
       {
           pIndVals->curr_callwait_ind = 1;
       }
       return 0;
    }
    return -1;
}

#ifdef PCM_ALSA
/*******************************************************************************
**
** Function         app_hs_open_alsa_duplex
**
** Description      function to open ALSA driver
**
** Parameters       None
**
** Returns          void
**
*******************************************************************************/
int app_hs_open_alsa_duplex(void) {
    int        status;
#ifdef ADANIS_ALSA_WRAPPER
    tAPP_ALSA_PLAYBACK_OPEN alsa_open;
    tBTA_SERVICE_MASK       open_services = BTA_HFP_HS_SERVICE_MASK;
    BOOLEAN is_using_I2S = FALSE;

    if(app_hs_cb.sco_route == BSA_SCO_ROUTE_PCM) {
        is_using_I2S = TRUE;
    }
#endif

#ifdef ADANIS_ALSA_WRAPPER
    adanis_alsa_init();
    adanis_alsa_playback_open_init(&alsa_open, open_services);
    //adanis_alsa_capture_open_init(alsa_open_capture);

    /* playback */
    APP_DEBUG1("Opening Alsa/Asound audio driver Playback rate:%d, is_using_I2S:%d", playback_rate, is_using_I2S);
    alsa_playback_opened = FALSE;
    alsa_open.blocking    = FALSE;
    alsa_open.format      = APP_ALSA_PCM_FORMAT_S16_LE;
    alsa_open.access      = APP_ALSA_PCM_ACCESS_RW_INTERLEAVED; /* PCM access */
    /* SEE I2S_CHANNELS */
    if( (APP_HS_CHANNEL_NB == 0x02) || (is_using_I2S == TRUE) ) {
        alsa_open.stereo      = TRUE;  /* Stereo */

        if((I2S_CHANNELS == 0x01) && (is_using_I2S == TRUE)) {
            alsa_open.stereo      = FALSE; /* Mono */
        }
    } else {
        alsa_open.stereo      = FALSE; /* Mono */
    }
    
    alsa_open.sample_rate = playback_rate; /* Sampling rate (e.g. 8000 for 8KHz) */
    alsa_open.latency     = 100000;        /* 100msec */

    if(alsa_handle_playback != NULL) {
        adanis_alsa_playback_close(open_services);
        alsa_handle_playback = NULL;
    }
    alsa_handle_playback = adanis_alsa_playback_open(&alsa_open, open_services, is_using_I2S);
    if(alsa_handle_playback == NULL) {
        APP_ERROR0("adanis_alsa_playback_open failed");
        return -1;
    }
    alsa_playback_opened = TRUE;


    /* capture */
    APP_DEBUG0("Opening Alsa/Asound audio driver Capture");
    alsa_capture_opened  = FALSE;
    if(alsa_handle_capture != NULL) {
        adanis_alsa_capture_close();
        alsa_handle_capture = NULL;
    }
    alsa_handle_capture = adanis_alsa_capture_open(&alsa_open, is_using_I2S);
    if(alsa_handle_capture == NULL) {
        APP_ERROR0("adanis_alsa_capture_open failed");
        return -1;
    }
    alsa_capture_opened = TRUE;

    if(is_using_I2S) {
#ifdef USE_I2S_SETUP_EXT_PARAMS
        status = adanis_i2s_setup_thread_alsa_SCO();
        if(status < 0x00) {
            app_hs_close_alsa_duplex();
        } else {
            app_hs_cb.audio_I2S_SCO_looping = TRUE;
        }
#else
        app_hs_cb.audio_I2S_SCO_looping = TRUE;
#endif
    }
#else /* else ADANIS_ALSA_WRAPPER */

    if(app_hs_cb.sco_route == BSA_SCO_ROUTE_PCM) {
#ifdef ADANIS_ALSA_LATENCY
        app_hs_cb.audio_I2S_SCO_looping = TRUE;

        /* NO NEED TO open alsa */
        return 0x00;
#endif
    }

    alsa_playback_opened = FALSE;

    /* If ALSA PCM driver was already open => close it */
    if (alsa_handle_playback != NULL) {
        snd_pcm_close(alsa_handle_playback);
        alsa_handle_playback = NULL;
    }

    APP_DEBUG0("Opening Alsa/Asound audio driver Playback");
    /* Open ALSA driver */
    status = snd_pcm_open(&alsa_handle_playback, alsa_device,
        SND_PCM_STREAM_PLAYBACK, SND_PCM_NONBLOCK);
    if (status < 0) {
        APP_ERROR1("snd_pcm_open failed: %s", snd_strerror(status));
        return status;
    } else {
        /* Configure ALSA driver with PCM parameters */
        status = snd_pcm_set_params(alsa_handle_playback,
            SND_PCM_FORMAT_S16_LE, SND_PCM_ACCESS_RW_INTERLEAVED,
            APP_HS_CHANNEL_NB, playback_rate/*APP_HS_SAMPLE_RATE*/, /* 2016/12/06 ADANIS YCJUNG */
            1, /* SW resample */
            100000);/* 100msec */
        if (status < 0) {
            APP_ERROR1("snd_pcm_set_params failed: %s", snd_strerror(status));
            return status;
        }
    }
    alsa_playback_opened = TRUE;
    
    alsa_capture_opened  = FALSE;
    /* If ALSA PCM driver was already open => close it */
    if (alsa_handle_capture != NULL) {
        snd_pcm_close(alsa_handle_capture);
        alsa_handle_capture = NULL;
    }
    APP_DEBUG0("Opening Alsa/Asound audio driver Capture");

    status = snd_pcm_open(&alsa_handle_capture, alsa_device,
        SND_PCM_STREAM_CAPTURE, SND_PCM_NONBLOCK);
    if (status < 0) {
        APP_ERROR1("snd_pcm_open failed: %s", snd_strerror(status));
        return status;
    } else {
        /* Configure ALSA driver with PCM parameters */
        status = snd_pcm_set_params(alsa_handle_capture,
            SND_PCM_FORMAT_S16_LE, SND_PCM_ACCESS_RW_INTERLEAVED,
            APP_HS_CHANNEL_NB, playback_rate/*APP_HS_SAMPLE_RATE*/, /* 2016/12/06 ADANIS YCJUNG */
            1, /* SW resample */
            100000);/* 100msec */
        if (status < 0) {
            APP_ERROR1("snd_pcm_set_params failed: %s", snd_strerror(status));
            return status;
        }
    }
    alsa_capture_opened = TRUE;
#endif /* endif ADANIS_ALSA_WRAPPER */

    return 0;
}

/*******************************************************************************
**
** Function         app_hs_close_alsa_duplex
**
** Description      function to close ALSA driver
**
** Parameters       None
**
** Returns          void
**
*******************************************************************************/
int app_hs_close_alsa_duplex(void) {
#ifdef ADANIS_ALSA_WRAPPER
    int               buffer_size;
    tBTA_SERVICE_MASK closing_services = BTA_HFP_HS_SERVICE_MASK;
#endif

    alsa_playback_opened = FALSE;
    alsa_capture_opened  = FALSE;

#ifdef ADANIS_ALSA_WRAPPER
    if(app_hs_cb.sco_route == BSA_SCO_ROUTE_PCM) {
        app_hs_cb.audio_I2S_SCO_looping = FALSE;

#ifdef USE_I2S_SETUP_EXT_PARAMS
        adanis_i2s_free_pcm_handle();
#endif

        buffer_size = (I2S_LATENCY_MAX * snd_pcm_format_width(I2S_FORMAT) / 0x08) * I2S_CHANNELS;

        memset(app_hs_cb.audio_i2s_buffer_b,   0x00, buffer_size);
        memset(app_hs_cb.audio_i2s_buffer,     0x00, buffer_size);
    }
    alsa_handle_capture  = NULL;
    alsa_handle_playback = NULL;

    APP_DEBUG0("Closing Alsa/Asound audio driver Playback");
    adanis_alsa_playback_close(closing_services);

    APP_DEBUG0("Closing Alsa/Asound audio driver Capture");
    adanis_alsa_capture_close();
#else /* else ADANIS_ALSA_WRAPPER */
    if(app_hs_cb.sco_route == BSA_SCO_ROUTE_PCM) {
#ifdef ADANIS_ALSA_LATENCY
        app_hs_cb.alsa_cp_stop_flag  = EX_ALSA_STOP;
        app_hs_cb.audio_I2S_SCO_looping = FALSE;
        return 0x00;
#endif
    } else {
        APP_DEBUG0("Closing Alsa/Asound audio driver Playback");
        if (alsa_handle_playback != NULL) {
            snd_pcm_close(alsa_handle_playback);
            alsa_handle_playback = NULL;
        }

        APP_DEBUG0("Closing Alsa/Asound audio driver Capture");
        if (alsa_handle_capture != NULL) {
            snd_pcm_close(alsa_handle_capture);
            alsa_handle_capture = NULL;
        }
    }
#endif /* endif ADANIS_ALSA_WRAPPER */
    return 0x00;
}
#endif /* PCM_ALSA */
